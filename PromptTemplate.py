from langchain.prompts import PromptTemplate
from langchain.chains import LLMChain
from langchain.chat_models import ChatOpenAI
from langchain.agents import AgentType,Tool,initialize_agent,AgentExecutor,ZeroShotAgent

todo_prompt = PromptTemplate.from_template(
    f"You are a planner who is an expert at coming up with a todo list for a given objective. Come up with a todo list for this objective: "
)

todo_chain = LLMChain(llm=ChatOpenAI(temperature=0), prompt=todo_prompt)
Tool(
    name="TODO",
    func=todo_chain.run,
    description="useful for when you need to come up with todo lists. Input: an objective to create a todo list for. Output: a todo list for that objective. Please be very clear what the objective is!",
)