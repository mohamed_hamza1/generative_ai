from langchain.schema import AgentAction, AgentFinish, OutputParserException
from langchain.agents import Tool, AgentOutputParser
from typing import List, Union
import re
from langchain.prompts import StringPromptTemplate


class CustomOutputParser(AgentOutputParser):

    def parse(self, llm_output: str) -> Union[AgentAction, AgentFinish]:
        # Check if agent should finish
        if "Final Answer:" in llm_output:
            return AgentFinish(
                # Return values is generally always a dictionary with a single `output` key
                # It is not recommended to try anything else at the moment :)
                return_values={"output": llm_output.split("Final Answer:")[-1].strip()},
                log=llm_output,
            )
        # Parse out the action and action input
        regex = r"Action\s*\d*\s*:(.*?)\nAction\s*\d*\s*Input\s*\d*\s*:[\s]*(.*)"
        match = re.search(regex, llm_output, re.DOTALL)
        if not match:
            raise OutputParserException(f"Could not parse LLM output: `{llm_output}`")
        action = match.group(1).strip()
        action_input = match.group(2)
        # Return the action and action input
        return AgentAction(tool=action, tool_input=action_input.strip(" ").strip('"'), log=llm_output)

class CustomPromptTemplate(StringPromptTemplate):
    # The template to use
    template: str
    # The list of tools available
    tools: List[Tool]

    def format(self, **kwargs) -> str:
        # Get the intermediate steps (AgentAction, Observation tuples)
        # Format them in a particular way
        intermediate_steps = kwargs.pop("intermediate_steps")
        thoughts = ""
        for action, observation in intermediate_steps:
            thoughts += action.log
            thoughts += f"\nObservation: {observation}\nThought: "
        # Set the agent_scratchpad variable to that value
        kwargs["agent_scratchpad"] = thoughts
        # Create a tools variable from the list of tools provided
        kwargs["tools"] = "\n".join([f"{tool.name}: {tool.description}" for tool in self.tools])
        # Create a list of tool names for the tools provided
        kwargs["tool_names"] = ", ".join([tool.name for tool in self.tools])
        return self.template.format(**kwargs)

class IntermediateStepPrompt:
    def __init__(self, tool): 
        # Set up the base template
        self.tool = tool
        self.template = """Answer the following questions as best you can, but speaking as a pirate might speak. You have access to the following tools:

        {tools}

        Use the following format:

        Question: the input question you must answer
        Thought: you should always think about what to do
        Action: the action to take, should be one of [{tool_names}]
        Action Input: the input to the action
        Observation: the result of the action
        ... (this Thought/Action/Action Input/Observation can repeat N times)
        Thought: I now know the final answer
        Final Answer: the final answer to the original input question

        Begin! Remember to speak as a formal person when giving your final answer.

        Question: {input}
        {agent_scratchpad}"""

    # Set up a prompt template    
    def generate_prompt(self):
        prompt = CustomPromptTemplate(
            template=self.template,
            tools=self.tool,
            # This omits the `agent_scratchpad`, `tools`, and `tool_names` variables because those are generated dynamically
            # This includes the `intermediate_steps` variable because that is needed
            input_variables=["input", "intermediate_steps"]
        )
        return prompt

class HistoryStepPrompt:
    def __init__(self, tool): 
        # Set up the base template
        self.tool = tool
        # Set up the base template
        self.template_with_history = """Answer the following questions as best you can, but don't generate answers out of the question scope. You have access to the following tools:
            {tools}

            Use the following format:

            Question: the input question you must answer
            Thought: you should always pass the question completely to one of [{tool_names}],
            Action: the action to take, should be one of [{tool_names}],
            Action Input: the input to the action, pass the objective as it's completely
            Observation: the result of the action
            ... (this Thought/Action/Action Input/Observation can repeat N times)
            Thought: I now know the final answer
            Final Answer: the final answer to the original input question

            Begin! Remember to speak as a formal person when giving your final answer.

            Previous conversation history:
            {history}

            New question: {input}
            {agent_scratchpad}"""
    # Set up a prompt template    
    def generate_prompt(self):
        prompt_with_history = CustomPromptTemplate(
            template=self.template_with_history,
            tools=self.tool,
            # This omits the `agent_scratchpad`, `tools`, and `tool_names` variables because those are generated dynamically
            # This includes the `intermediate_steps` variable because that is needed
            input_variables=["input", "intermediate_steps", "history"]
        )
        return prompt_with_history
