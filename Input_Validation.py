import openai
import os
from dotenv import load_dotenv
load_dotenv()

# Replace 'YOUR_API_KEY' with your actual OpenAI API key
api_key = os.environ.get('OPENAI_API_KEY')
# Function to check if user input matches a specific description
class validate:
    def check_description(user_input, description):
        openai.api_key = api_key

        # Create a prompt with user input and the description to check
        messages = [
            {"role": "system", "content": f"You are a helpful assistant that respond with only true or false. find from the following paramters if the user input matches the description , make sure that the response must be true or false"},
            {"role": "user", "content": f"The following paramters:  User: {user_input}\n Description:  {description}\n "},
        ]
        # Use the GPT-3 model to generate a response
        response = openai.ChatCompletion.create(
            # model="text-davinci-002",
            model = "gpt-3.5-turbo-16k",
            messages=messages,
            temperature = 0
        )
        

        # Get the generated response
        generated_response = response['choices'][0]['message']['content']

        # generated_response = response.choices[0].text.strip().lower()
        # Check if the response is 'yes' or 'true'
        print(generated_response)
        if generated_response.lower() == "false":
            generated_response = False
            return generated_response
        elif generated_response.lower() == "true":
            generated_response = True
            return generated_response
        else: 
            return "None"