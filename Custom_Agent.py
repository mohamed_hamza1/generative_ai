from langchain.agents import Tool, AgentExecutor, LLMSingleActionAgent, AgentOutputParser
from langchain.chat_models import ChatOpenAI
from langchain.chains import LLMChain
from Prompt_Parser import *
from dotenv import load_dotenv
import os
import time
from langchain.memory import ConversationBufferWindowMemory
# from Packages.Unstructured_Documents.Excel import excel_with_agent
from Packages.Data_Scrapper import Scrapper_Agent


from Packages.Document.Unstructured.Excel import excel_with_agent
from Packages.Document.Unstructured.PDF import pdf_with_agent
from Packages.Document.Unstructured.Word import word_with_agent 
from Packages.Document.Unstructured.Powerpoint import powerpoint_with_agent
# from Packages.Document.Unstructured.CSV.csv_with_agent import csv_agent_r

from Packages.Document.Structured import structured_documents_agent

from Packages.MS_Office.MSOffice.Word_Agent import WordAgentTool
from Packages.MS_Office.MSOffice.PDF_Agent import PDFAgentTool
from Packages.MS_Office.MSOffice.PowerPoint_Agent import PowerPintAgentTool
from Packages.MS_Office.MSOffice.Excel_Agent import ExcelAgentTool
from Packages.MS_Office.MSOffice.GoogleDocs_Agent import GoogleDocsAgentTool
from Packages.MS_Office import MicrosoftOutlookAgent, MSOutlookCalendarAgent, Onedrive_Agent
from Packages.Google.GoogleAgent import GoogleAgentTool
# from Packages.Jira.JiraAgent import JiraAgentTool
# from Packages.Trello.TrelloAgent import TrelloAgentTool
from Packages.SearchTool import SerpAPIAgentTool
from Input_Validation import validate
# Load environment variables from .env file
load_dotenv()
# Define which tools the agent can use to answer user queries

search = SerpAPIAgentTool()
# Excel_Retrieving_Tool = excel_with_agent.ExcelRetrievingTool()
MS_Office_outlook = MicrosoftOutlookAgent.MicrosoftOutlookAgentTool()
MS_office_calendar = MSOutlookCalendarAgent.MicrosoftCalendarOutlookAgentTool()
Google_Apps = GoogleAgentTool()
# Jira_Agent_Tool = JiraAgentTool()
# Trello_Agent_Tool = TrelloAgentTool()
PowerPoint_Agent_Tool=PowerPintAgentTool()
# Scrapper_Agent_Tool = ScrapperAgentTool()
Word_Agent_Tool=WordAgentTool()
PDF_Agent_Tool=PDFAgentTool()
Excel_Agent_Tool=ExcelAgentTool()
Google_Docs_Agent=GoogleDocsAgentTool()
# Onedrive_Agent_Tool = Onedrive_Agent.OneDriveAgentTool()
tools = []
tools = [
    Tool(
        name="Search",
        func=search.run,
        description=search.get_description(),
    ),
    Tool(
        name = "MS_Office_Outlook_Agent",
        func = MS_Office_outlook.run,
        description=MS_Office_outlook.get_description()
    ),
    Tool(
        name = "MS_Office_Calendar_Outlook_Agent",
        func = MS_office_calendar.run,
        description=MS_office_calendar.get_description()
    ),
    Tool(
        name = "Google_Apps_Agent",
        func = Google_Apps.run,
        description=Google_Apps.get_description()
    ),
   
    # Tool(
    #     name = "Excel_Retrieving_Agent",
    #     func = Excel_Retrieving_Tool.run,
    #     description=Excel_Retrieving_Tool.get_description()
    # ),
    # Tool(
    #     name = "Scrapper_Agent_Tool",
    #     func = Scrapper_Agent_Tool.run,
    #     description=Scrapper_Agent_Tool.get_description()
    # ),
     Tool(
        name = "PowerPoint_Agent_Tool",
        func = PowerPoint_Agent_Tool.run,
        description=PowerPoint_Agent_Tool.get_description()
    ),
    Tool(
        name = "WordAgentTool",
        func = Word_Agent_Tool.run,
        description=Word_Agent_Tool.get_description()
    ),
    Tool(
        name = "PDFAgentTool",
        func = PDF_Agent_Tool.run,
        description=PDF_Agent_Tool.get_description()
    ),
    Tool(
        name = "ExcelAgentTool",
        func = Excel_Agent_Tool.run,
        description=Excel_Agent_Tool.get_description()
    ),
     Tool(
        name = "GoogleDocsAgentTool",
        func = Google_Docs_Agent.run,
        description=Google_Docs_Agent.get_description()
    ),
    # Tool(
    #     name = "OneDrive_Agent_Tool",
    #     func = Onedrive_Agent_Tool.run,
    #     description=Onedrive_Agent_Tool.get_description()
    # )
    
]
Task_management_Tools = [ 
        # Tool(
        # name = "Jira_Agent",
        # func = Jira_Agent_Tool.run,
        # description=Jira_Agent_Tool.get_description()
        # ),
        # Tool(
        # name = "Trello_Agent",
        # func = Trello_Agent_Tool.run,
        # description=Trello_Agent_Tool.get_description()
        # )
    ]
output_parser = CustomOutputParser()
# prompt = IntermediateStepPrompt(tools).generate_prompt()
prompt = HistoryStepPrompt(tools).generate_prompt()

llm = ChatOpenAI(model_name="gpt-3.5-turbo-16k",temperature=0)
# llm = ChatOpenAI(model_name="gpt-4",temperature=0)

# LLM chain consisting of the LLM and a prompt
llm_chain = LLMChain(llm=llm, prompt=prompt)

tool_names = [tool.name for tool in tools]
agent = LLMSingleActionAgent(
    llm_chain=llm_chain,
    output_parser=output_parser,
    stop=["\nObservation:"],
    allowed_tools=tool_names
)
memory=ConversationBufferWindowMemory(k=2)

from langchain.callbacks import get_openai_callback

agent_executor = AgentExecutor.from_agent_and_tools(agent=agent, tools=tools, verbose=True, memory=memory,handle_parsing_errors=True)
    
    

import threading

# This function will be called after timeout and will raise an exception
def input_timeout():
    raise TimeoutError

# This is the main function asking for user input
def get_input(prompt, timeout=10):
    # Create a timer that will call input_timeout after `timeout` seconds
    timer = threading.Timer(timeout, input_timeout)
    try:
        print(prompt)
        timer.start()
        return input()
    except TimeoutError:
        print("Input timed out.")
        exit()
    finally:
        timer.cancel()

if __name__ == '__main__':
    try:
        # Assign the user plan from the Model
        user_plan = "Pro"
        
        structure_process = False
        unstructure_process = False
        
        # if user_plan.lower() == "trial":
        #     tools = basic_tools + Task_management_Tools
        #     structure_process = True
        #     unstructure_process = True
        
        # elif user_plan.lower() == "basic":
        #     tools = basic_tools
        #     structure_process = False
        #     unstructure_process = False
        
        # elif user_plan.lower() == "lite":
        #     tools = basic_tools + Task_management_Tools
        #     structure_process = False
        #     unstructure_process = True
        # elif user_plan.lower() == "pro":
        #     tools = basic_tools + Task_management_Tools
        #     structure_process = True
        #     unstructure_process = True
        
        while True:
            user_input = get_input("Enter your request:", 5000)
           
            if not user_input:
                print("Exiting due to no input.")
                break
            elif validate.check_description(user_input,"The user would be asking for an extract , scrape , qury or retrieve  data or information from the given URL or document URL like (PDF,Excel, Word,Power Point) documents  ") and unstructure_process :
                from Questioning import ask_bot
                ask_bot.ask_user(user_input)
            else:
                print("main Agent")
                start_time = time.time()
                with get_openai_callback() as cb:
                    response = agent_executor.invoke({"input": user_input})
                    # Print the output
                    print("Result: "+response["output"])
                    # Access the token usage information
                    main_agent_tokens = cb.total_tokens
                    # print(response)
                    from Packages.Agents.Init_Agent import initialize_agent_tool
                    agent = initialize_agent_tool()
                    # Total Tokens used by agents
                    total_tokens = agent.Current_Tokens() + main_agent_tokens
                    print(f"Total Current Tokens: {total_tokens}")
                    end_time = time.time()
                    elapsed_time = end_time-start_time
                    print(f"agent took {elapsed_time:.2f} seconds to run")
    except KeyboardInterrupt:
        print("\nExiting the program.")



