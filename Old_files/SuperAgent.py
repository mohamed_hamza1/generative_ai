import os
import time
from langchain.vectorstores import FAISS
import faiss
from langchain.docstore import InMemoryDocstore
from langchain.embeddings import OpenAIEmbeddings
from langchain_experimental.autonomous_agents import AutoGPT
from langchain.chat_models import ChatOpenAI
from Microsoft.MicrosoftAgent import MicrosoftAgentTool

os.environ["OPENAI_API_KEY"] = "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"

objective = ["Get the last 10 emails from my account and forward it to mohamed.hamza@smarttechsys.com, and add menna.emad@smarttechsys.com to the CC, where the body should contain the result of the found emails subjects"]

tools = [MicrosoftAgentTool()]


# Define your embedding model
embeddings_model = OpenAIEmbeddings()
embedding_size = 1536
index = faiss.IndexFlatL2(embedding_size)
vectorstore = FAISS(embeddings_model.embed_query, index, InMemoryDocstore({}), {})

llm = ChatOpenAI(model_name="gpt-3.5-turbo-16k", temperature=0)

agent = AutoGPT.from_llm_and_tools(
    ai_name="Hamza",
    ai_role="Assistant",
    tools=tools,
    llm=llm,
    memory=vectorstore.as_retriever(),
    human_in_the_loop=True, # Set to True if you want to add feedback at each step.
    # memory=vectorstore.as_retriever(search_kwargs={"k": 8}),
    # chat_history_memory=FileChatMessageHistory("chat_history.txt"),
)
agent.chain.verbose = True

start_time = time.time()

print(objective)
agent.run(objective)

end_time = time.time()
elapsed_time = end_time-start_time
print(f"agent took {elapsed_time:.2f} seconds to run")