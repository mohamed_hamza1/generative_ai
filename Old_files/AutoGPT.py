import os
import time
from langchain.vectorstores import FAISS
import faiss
from langchain.docstore import InMemoryDocstore
from langchain.embeddings import OpenAIEmbeddings
from langchain_experimental.autonomous_agents import AutoGPT
from langchain.chat_models import ChatOpenAI
from dotenv import load_dotenv
from Packages.Google.GoogleAgent import *
from Packages.Jira.JiraAgent import *
from Packages.MS_Office.MicrosoftOutlookAgent import *
from Packages.Trello.TrelloAgent import *
from langchain.tools import WriteFileTool,ReadFileTool
from Packages.SearchTool import SerpAPIAgentTool
load_dotenv()

tools=[
    GoogleAgentTool(),
    JiraAgentTool(),
    MicrosoftOutlookAgentTool(),
    TrelloAgentTool(),
    WriteFileTool(),
    ReadFileTool(),
    SerpAPIAgentTool()
    
]
# Define your embedding model
embeddings_model = OpenAIEmbeddings()
embedding_size = 1536
index = faiss.IndexFlatL2(embedding_size)
vectorstore = FAISS(embeddings_model.embed_query, index, InMemoryDocstore({}), {})

# max_iterations: Optional[int] = 3


# llm = ChatOpenAI(model_name="gpt-4", temperature=0)
llm=ChatOpenAI(model="gpt-3.5-turbo-16k",temperature=0)


# agent = AutoGPT.from_llm_and_tools(
#     ai_name="Hamza",
#     ai_role="Assistant",
#     tools=tools,
#     llm=llm,
#     memory=vectorstore.as_retriever(),
#     # memory=vectorstore.as_retriever(search_kwargs={"k": 8}),
#     # human_in_the_loop=True, # Set to True if you want to add feedback at each step.
#     # chat_history_memory=FileChatMessageHistory("chat_history.txt"),
# )
# agent.chain.verbose = True

agent = initialize_agent(
    tools=tools,
    llm=llm,
    agent=AgentType.STRUCTURED_CHAT_ZERO_SHOT_REACT_DESCRIPTION, 
    verbose=True
    )
start_time = time.time()
# objective = input("Input: ")
objective = ("generate a report regarding the weather in Cairo, and save the file as Cairo_report.txt")

print(objective)
agent.run(objective)

end_time = time.time()
elapsed_time = end_time-start_time
print(f"agent took {elapsed_time:.2f} seconds to run")