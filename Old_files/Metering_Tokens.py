import openai
import tiktoken
import os
from dotenv import load_dotenv
load_dotenv()
# Assuming you've already set up your OpenAI API key
# openai.api_key = 'YOUR_OPENAI_API_KEY'
TOKEN_LIMIT = 10000  # Set your desired token limit
current_tokens_used = 0

def num_tokens_from_string(string: str, encoding_name: str) -> int:
    encoding = tiktoken.encoding_for_model(encoding_name)
    num_tokens = len(encoding.encode(string))
    return num_tokens

def call_chatGPT(prompt):
    global current_tokens_used

    prompt_token_count = num_tokens_from_string(prompt, "gpt-3.5-turbo")  # Assuming you're using the gpt-3.5-turbo model
    print(f"used_tokens_No. {prompt_token_count}")
    env_value = os.environ.get('OPENAI_API_KEY')
    print(env_value)

    response = openai.Completion.create(
        api_key = "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",
        engine="davinci",
        prompt=prompt,
        max_tokens=150  # Set a reasonable default, or customize as needed
    )
    
    response_token_count = num_tokens_from_string(response.choices[0].text, "gpt-3.5-turbo")

    total_tokens_for_this_call = prompt_token_count + response_token_count

    # Check if we exceed the limit
    if current_tokens_used + total_tokens_for_this_call > TOKEN_LIMIT:
        raise Exception("Token limit exceeded!")

    current_tokens_used += total_tokens_for_this_call

    return response.choices[0].text

# Testing the function
try:
    response = call_chatGPT("Write a 15000 word regarding inflation and quantum mechanic")
    print(response)
except Exception as e:
    print(str(e))
