from langchain.agents import AgentType
from langchain.chat_models import ChatOpenAI
from langchain.agents import initialize_agent
import os
from Packages.stock.CurrentStockPriceTool import CurrentStockPriceTool
from Packages.stock.StockPerformanceTool import StockPerformanceTool


os.environ["OPENAI_API_KEY"] = "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"

llm = ChatOpenAI(model="gpt-3.5-turbo-0613", temperature=0)

tools = [CurrentStockPriceTool(), StockPerformanceTool()]

agent = initialize_agent(tools, llm, agent=AgentType.OPENAI_FUNCTIONS, verbose=True)

agent.run(
    "What is the current price of Microsoft stock? How it has performed over past 6 months?"
)

# print(CurrentStockPriceTool.get_current_stock_price("MSFT"))
# print(StockPerformanceTool.get_stock_performance("MSFT", 30))