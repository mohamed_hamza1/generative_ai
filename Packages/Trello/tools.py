import json
import os
from typing import Type
from pydantic import BaseModel, Field
from langchain.tools import BaseTool
from dotenv import load_dotenv
import requests

load_dotenv()
token = os.getenv('TOKEN')


# Get User Information
def trello_me_api():
    url = f"http://127.0.0.1:8000/trello-management/trello-me/?APIToken={token}"
    headers = {"Accept": "application/json"}
    response = requests.request("GET",url,headers=headers)
    response_data = response.json()
    return response_data

class GetUserInfoTool(BaseTool):
    name = "TrelloInput"
    description = "This tool is provides Trello user account information, including board details."
    
    def _run(self,query:str)->json:
        response = trello_me_api()
        return response
    
    async def _arun(self,query:str)->str:
        raise NotImplementedError("not supported")


# Create Board in Project
def trello_board_creation(name):
    url = f"http://127.0.0.1:8000/trello-management/trello-board/"
    query = {"name":name,'token': token}
    print(query)
    headers = {"Accept": "application/json"}
    response = requests.request("POST",url,headers=headers,data=query)
    response_data = response.json()
    return response_data

class BoardCreationInputSchema(BaseModel):
    name: str = Field(description="It should be a trello board name")

class BoardCreationTool(BaseTool):
    name = "board_creation"
    description = "This tool is a custom tool that allows you to create a new Trello board with the specified name."
    args_schema: Type[BoardCreationInputSchema] = BoardCreationInputSchema
    
    def _run(self,name:str)->json:
        response = trello_board_creation(name)
        return response
    
    async def _arun(self,name:str)->str:
        raise NotImplementedError("not supported")  

# Create a list in Project
def trello_list_creation(name,board_id):
    url = f"http://127.0.0.1:8000/trello-management/trello-list/"
    query = {"name":name,"token": token,"board_id":board_id}
    headers = {"Accept": "application/json"}
    response = requests.request("POST",url,headers=headers,data=query)
    response_data = response.json()
    return response_data

class ListCreationInputSchema(BaseModel):
    name: str = Field(description="It should be a trello list name")
    board_id: str = Field(description="It should be a trello board id")
    
class ListCreationTool(BaseTool):
    name = "list_creation"
    description = "This tool allows you to create a new Trello list on the specified board with the specified name."
    args_schema: Type[ListCreationInputSchema] = ListCreationInputSchema
    
    def _run(self,name:str,board_id:str)->json:
        response = trello_list_creation(name,board_id)
        return response
    
    async def _arun(self,name:str,board_id:str)->str:
        raise NotImplementedError("not supported")  


# Create a card in list
def trello_card_creation(name,idList,description,due):
    url = f"http://127.0.0.1:8000/trello-management/trello-card/"
    query = {
             "token": token,
             "list_id":idList,
             "name":name,
             "description":description,
             "due":due}
    
    headers = {"Accept": "application/json"}
    response = requests.request("POST",url,headers=headers,data=query)
    response_data = response.json()
    return response_data

class CardCreationInputSchema(BaseModel):
    name: str = Field(description="It should be a trello card name")
    idList: str = Field(description="It should be a trello list id")
    description: str = Field(description="It should be the description for the card")
    due: str = Field(description="It should be a due date for the card")
    
class CardCreationTool(BaseTool):
    name = "card_creation"
    description = "This tool allows you to create a new Trello card on the specified list with the specified name, description and due date."
    args_schema: Type[CardCreationInputSchema] = CardCreationInputSchema
    
    def _run(self,name:str,idList:str,description:str,due:str)->json:
        print(name,idList,description,due)
        response = trello_card_creation(name,idList,description,due)
        return response
    
    async def _arun(self,name:str,idList:str,description:str,due:str)->str:
        raise NotImplementedError("not supported")  

# Get Member by username
def trello_get_member(username):
    url = f"http://127.0.0.1:8000/trello-management/trello-member/"
    query = {
             "token": token,
             "username":username,
             }
    
    headers = {"Accept": "application/json"}
    response = requests.request("GET",url,headers=headers,data=query)
    response_data = response.json()
    return response_data

class GetMemebernInputSchema(BaseModel):
    username: str = Field(description="It should be a trello member username or Id")
    
class GetMemberTool(BaseTool):
    name = "get_member"
    description = "The Trello member ID retrieval tool with error handling is a custom tool that gets the ID of a member using the username, and returns an error if the user is not linked with the Trello board."
    args_schema: Type[GetMemebernInputSchema] = GetMemebernInputSchema
    
    def _run(self,username:str)->json:
        response = trello_get_member(username)
        return response
    
    async def _arun(self,username:str)->str:
        raise NotImplementedError("not supported")  


##############################
# Invite Member by email
def trello_email_member(email,board_id,name):
    url = "http://127.0.0.1:8000/trello-management/trello-member/"
    query = {
             "token": token,
             "email":email,
             "board_id":board_id,
             "name":name
             }
    
    headers = {"Accept": "application/json"}
    response = requests.request("PUT",url,headers=headers,data=query)
    response_data = response.json()
    return response_data

class EmailMemebernInputSchema(BaseModel):
    email: str = Field(description="It should be a member email")
    board_id: str = Field(description="It should be a board_id")
    name: str = Field(description="It should be a member username")
    
class EmailMemberTool(BaseTool):
    name = "invite_member"
    description = "The Trello member invitation tool is a custom tool that invites a Trello member to a board by email address."
    args_schema: Type[EmailMemebernInputSchema] = EmailMemebernInputSchema
    
    def _run(self,email:str,board_id:str,name:str)->json:
        response = trello_email_member(email,board_id,name)
        return response
    
    async def _arun(self,email:str,board_id:str,name:str)->str:
        raise NotImplementedError("not supported")  


##############################
# Add a Member to a Card
def trello_add_member(card_id,member_id):
    url = "http://127.0.0.1:8000/trello-management/trello-member/"
    query = {
             "token": token,
             "card_id":card_id,
             "member_id":member_id
             }
    
    headers = {"Accept": "application/json"}
    response = requests.request("POST",url,headers=headers,data=query)
    response_data = response.json()
    return response_data

class AddMemebernInputSchema(BaseModel):
    card_id: str = Field(description="It should be a card id of board list")
    member_id: str = Field(description="It should be a member id")
    
class AddMemberTool(BaseTool):
    name = "add_member"
    description = "The Trello card assignment tool is a custom tool that assigns a Trello card to a member"
    args_schema: Type[AddMemebernInputSchema] = AddMemebernInputSchema
    
    def _run(self,card_id:str,member_id:str)->json:
        response = trello_add_member(card_id,member_id)
        return response
    
    async def _arun(self,card_id:str,member_id:str)->str:
        raise NotImplementedError("not supported")  


