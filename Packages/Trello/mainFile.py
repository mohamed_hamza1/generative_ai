from langchain.agents import AgentType,initialize_agent
from langchain.chat_models import ChatOpenAI
from dotenv import load_dotenv
from tools import *
load_dotenv()

# tools 
tool = [GetUserInfoTool(),BoardCreationTool(),ListCreationTool(),CardCreationTool(),GetMemberTool(),EmailMemberTool(),AddMemberTool()]
llm = ChatOpenAI(model="gpt-3.5-turbo-0613", temperature=0)
agent = initialize_agent(tools=tool,
                         llm=llm,
                         agent=AgentType.OPENAI_FUNCTIONS,
                         verbose = True
                         )
agent.run("""Create a new board called test'park with a list called urgent_task. 
          Add a new card to the list with the following information:
          Card name: Bugs needs to be fixed ,
          Description: This bug causes an issue in payment, 
          Due date: 10 Nov 2023
          Also invite jiyo yopi jijo.yopi@oodles.io to a board and add his/her to this card """)