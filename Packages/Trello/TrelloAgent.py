from typing import Type
from pydantic import BaseModel, Field
from langchain.tools import BaseTool
from langchain.agents import AgentType,Tool,initialize_agent
from langchain.chat_models import ChatOpenAI
from dotenv import load_dotenv
from Packages.Trello.tools import *
load_dotenv()

def Trello_Agent(objective):
    """Method to call Trello agent toolkit"""
    tools = [GetUserInfoTool(),BoardCreationTool(),ListCreationTool(),CardCreationTool(),GetMemberTool(),EmailMemberTool(),AddMemberTool()]
    prompt = """
        You are an assistant that helps create projects and issues. You should always:
        - Authenticate the user before taking any other action. You can use AuthenticateUserTool.
        - Be polite and helpful
        - Get user info out through trello GetUserInfoTool using the provided tools when asked
        - Create board through trello BoardCreationTool using the provided tools when asked
        - Create List through trello ListCreationTool using the provided tools when asked
        - Create Card  through trello CardCreationTool using the provided tools when asked
        - Get member info through trello GetMemberTool using the provided tools when asked
        - Send Email to memeber through trello EmailMemberTool using the provided tools when asked
        - Add memeber through trello AddMemberTool using the provided tools when asked
        - Provide explanations for your actions
        User: {input}
        Assistant:
        """
    from Packages.Agents.Init_Agent import initialize_agent_tool
    agent = initialize_agent_tool()
    response = agent.init_agent(tools,prompt,objective,AgentType.STRUCTURED_CHAT_ZERO_SHOT_REACT_DESCRIPTION)

    return (response)
 
class TrelloAgentToolInput(BaseModel):
    """Inputs for Trello_Agent"""

    objective: str = Field(description="the objective required for agent prompt input")

  
class TrelloAgentTool(BaseTool):
    name = "Trello_Agent"
    description = """
        The Trello Apps Toolkit is designed for efficient system integrations. By initializing the Trello agent, users gain access to a suite of utilities including the GetUserInfoTool(),BoardCreationTool(),ListCreationTool(),CardCreationTool(),GetMemberTool(),EmailMemberTool(),AddMemberTool(). These tools streamline processes, enhancing user experience and operational effectiveness within the Trello environment.
        """
    args_schema: Type[BaseModel] = TrelloAgentToolInput
    def get_description(self):
        return self.description
    def _run(self, objective: str) -> str:
        response = Trello_Agent(objective)
        return response

    def _arun(self, objective: str):
        raise NotImplementedError("Trello_Agent does not support async")