from typing import Type
from pydantic import BaseModel, Field
from langchain.tools import BaseTool
from langchain.agents import Tool,AgentType
from langchain.utilities import SerpAPIWrapper
from dotenv import load_dotenv
import os
load_dotenv()


def SerpAPI_Agent(objective):
    """Method to call Search agent toolkit"""
    os.environ["SERPAPI_API_KEY"] = "42515267f25504cefb9c91dfacd6eec57f2f794b9dc122714edb8e57dd37ed2b"
    
    search = SerpAPIWrapper()
    tools = [Tool(name="search",func=search.run,description="useful for when you need to answer questions about current events. You should ask targeted questions")]
    prompt = """
        You are an assistant that helps to get access to microft outlook tools. You should always:
        - Take the user input, and list all the provided arguments within the user input.
        - Pass the arguments to the provided tools when being used
        - Be polite and helpful
        - Search for whatever question that user ask for
        - return only the most accurate result with a structured point
        - Provide explanations for your actions
        User: {input}
        Assistant:
        """
    from Packages.Agents.Init_Agent import initialize_agent_tool
    agent = initialize_agent_tool()
    response = agent.init_agent(tools,prompt,objective,AgentType.STRUCTURED_CHAT_ZERO_SHOT_REACT_DESCRIPTION)

    return (response)
 
class SerpAPIAgentToolInput(BaseModel):
    """Inputs for SerpAPI_Agent"""

    objective: str = Field(description="the objective required for agent prompt input")

  
class SerpAPIAgentTool(BaseTool):
    name = "SerpAPI_Agent"
    description = """
        Useful when you want to initialize the agent for Search API that uses useful Searp API when you need to answer questions about current events. You should ask targeted questions
        """
    args_schema: Type[BaseModel] = SerpAPIAgentToolInput
    def get_description(self):
        return self.description
    
    def _run(self, objective: str) -> str:
        response = SerpAPI_Agent(objective)
        return response

    def _arun(self, objective: str):
        raise NotImplementedError("SerpAPI_Agent does not support async")
    