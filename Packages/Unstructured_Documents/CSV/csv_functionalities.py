from langchain.agents import create_csv_agent
from langchain.llms import OpenAI
import requests
import os

os.environ["OPENAI_API_KEY"] = "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"

def download_sheet_as_csv(link, destination):
    
    # Extract the file ID from the link
    file_id = link.split('/spreadsheets/d/')[1].split('/edit')[0]
    
    # Create the direct download link for CSV format
    direct_link = f"https://docs.google.com/spreadsheets/d/{file_id}/export?format=csv"

    # Make the request and save the content in the specified destination
    response = requests.get(direct_link, stream=True)
    
    response.raise_for_status()
    with open(destination, "wb") as f:
        for chunk in response.iter_content(chunk_size=32768):
            f.write(chunk)
            
    
def query_csv_file(url):
    print(url)
    try:
        out_csv_folder = "csvOutput"
        file_name = "downloadedCSV.csv"
        if not os.path.exists(out_csv_folder):
            os.makedirs(out_csv_folder)
        local_path = os.path.abspath(f"{out_csv_folder}/{file_name}")
        print("===============local path", local_path)
        download_sheet_as_csv(url, local_path)
        # df = pd.read_csv('/content/train.csv')
        # agent = create_csv_agent(OpenAI(temperature=0), 
        #                         local_path, 
        #                         verbose=True)     
        # answer = agent.run(query_prompt) 
        return local_path 
    except Exception as e:
        print("error", e)
                    
def query_csv_file_with_prompt(query_prompt):
    try:
        out_csv_folder = "csvOutput"
        file_name = "downloadedCSV.csv"
        if not os.path.exists(out_csv_folder):
            os.makedirs(out_csv_folder)
        local_path = os.path.abspath(f"{out_csv_folder}/{file_name}")
        
        agent = create_csv_agent(OpenAI(temperature=0), 
                                local_path, 
                                verbose=True)     
        answer = agent.run(query_prompt) 
        return answer 
    except Exception as e:
        print("error", e)