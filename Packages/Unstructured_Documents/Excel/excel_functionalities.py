from langchain.agents import create_pandas_dataframe_agent, AgentType
from langchain.chat_models.openai import ChatOpenAI
from dotenv import load_dotenv
import pandas as pd
import requests
import sys
import os 

# __import__('pysqlite3')

# sys.modules['sqlite3'] = sys.modules.pop('pysqlite3')

load_dotenv()

def download_sheet_as_excel(link, destination):
    # Extract the file ID from the link
    print("==============destination", destination)
    file_id = link.split('/spreadsheets/d/')[1].split('/edit')[0]
    print("==========downloading the excel sheet =================")
    # Create the direct download link for Excel format
    direct_link = f"https://docs.google.com/spreadsheets/d/{file_id}/export?format=xlsx"

    # Make the request and save the content in the specified destination
    response = requests.get(direct_link, stream=True)
    response.raise_for_status()
    with open(destination, "wb") as f:
        for chunk in response.iter_content(chunk_size=32768):
            f.write(chunk)

def excel_file_retrieving(url):
    try:
        out_excel_folder = "Packages/Unstructured_Documents/Excel/excelOutput"
        file_name = "downloadedExcel.xlsx"
        if not os.path.exists(out_excel_folder):
            os.makedirs(out_excel_folder)
        local_path = os.path.abspath(f"{out_excel_folder}/{file_name}")

        download_sheet_as_excel(url, local_path)
                
        df = pd.read_excel(local_path)
        # print("=========df", df)
        # agent = create_pandas_dataframe_agent(
        #     ChatOpenAI(temperature=0, model="gpt-3.5-turbo"),
        #     df,
        #     verbose=True,
        #     agent_type=AgentType.OPENAI_FUNCTIONS,
        # )
          
        # answer = agent.run(query_prompt) 
        return df 
    except Exception as e:
        print("error", e)
                    
     