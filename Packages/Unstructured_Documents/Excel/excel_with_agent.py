from langchain.agents import AgentType,initialize_agent
from Packages.Unstructured_Documents.Excel.excel_functionalities import excel_file_retrieving 
from langchain.chat_models import ChatOpenAI
from pydantic import BaseModel, Field
from langchain.tools import BaseTool
from dotenv import load_dotenv
from typing import Type
import os 

load_dotenv()

class RetrievingExcelInput(BaseModel):
    """Inputs for excel_file_retrieving"""

    URL: str = Field(description="The web address or path where the Excel document resides")
    # objective: str = Field(description="the objective required for agent prompt input")

class ExcelRetrievingTool(BaseTool):
    name = "excel_file_retrieving"
    description = """
    This function downloads an Excel file from Google Drive, scans it with a defined search string to find specific data, 
    and presents the results in JSON format for easy access and further use.
    """
    args_schema: Type[BaseModel] = RetrievingExcelInput
    def get_description(self):
        return self.description
    def _run(self, URL: str):
        print("========url", URL)
        response = excel_file_retrieving(URL)
        print("function response", response)
        return response

# # Set up the turbo LLM
# turbo_llm = ChatOpenAI(
#     temperature=0,
#     model_name='gpt-3.5-turbo'
# )

        
# tools = [ExcelProcessingTool()]
# # create our agent
# agent = initialize_agent(
#     tools=tools,
#     llm=turbo_llm,
#     agent=AgentType.OPENAI_MULTI_FUNCTIONS,
#     verbose=True,
# )

# agent_output = agent.run("from the following URL https://docs.google.com/spreadsheets/d/1-JrlVAxjNTGrXZVOkUohYgS0l-jpoqV5/edit?usp=sharing&ouid=104825586842121347927&rtpof=true&sd=true, please give me first name and last name and age of id 2579")
# print(agent_output)

