from typing import Type
from pydantic import BaseModel, Field
from langchain.tools import BaseTool
from langchain.agents import Tool
from dotenv import load_dotenv
import os
from langchain.agents import AgentType
from Packages.Unstructured_Documents.Excel import excel_with_agent


load_dotenv()


def Excel_Retrieving_Agent(objective):
    """Method to call Excel_retrieving agent toolkit"""
    
    tools = [excel_with_agent.ExcelRetrievingTool()]
    prompt ="""
        You are an assistant that helps to get access to Excel documents tools. You should always:
        - Take the user input, and list all the provided arguments within the user input.
        - Pass the arguments to the provided tools as it is when being used
        - Be polite and helpful
        - Don't create any sql or database queries, pass the prompt as it is without URLs 
        - Pass the Excel URL and pass the rest of the prompt or the objective as a seperate argument to ExcelRetrievingTool 
        - Provide explanations for your actions
        User: {input}
        Assistant:
        """
        
    from Packages.Agents.Init_Agent import initialize_agent_tool
    agent = initialize_agent_tool()
    response = agent.init_agent(tools,prompt,objective,AgentType.OPENAI_MULTI_FUNCTIONS)
    return (response)
 
class Excel_Retrieving_AgentToolInput(BaseModel):
    """Inputs for Excel_Retrieving_Agent"""

    objective: str = Field(description="the objective required for agent prompt input")

  
class Excel_Retrieving_AgentTool(BaseTool):
    name = "Excel_Retrieving_Agent"
    description = """
        Useful when you want to initialize the agent for Excel retrieving that uses excel file for retrieving when you need to answer questions about current events. You should ask targeted questions
        """
    args_schema: Type[BaseModel] = Excel_Retrieving_AgentToolInput
    def get_description(self):
        return self.description
    
    def _run(self, objective: str) -> str:
        response = Excel_Retrieving_Agent(objective)
        return response
