from typing import Type
from pydantic import BaseModel, Field
from langchain.tools import BaseTool
from langchain.agents import AgentType,Tool,initialize_agent
from langchain.chat_models import ChatOpenAI
from Packages.Data_Scrapper.ScrapperTool import CreateScrapperTool
from dotenv import load_dotenv
load_dotenv()

def Scrapper_Agent(objective):
    """Method to call Scrapper agent toolkit"""
    tools = [CreateScrapperTool()]
    prompt = """
        You are an assistant that helps extarct data (fileds) from any website this tool is using for scrappping data. You should always:
        - Be polite and helpful
        - Create Scrapping toool using the provided tools when asked
        - Must return the final Observation as a result .
        - Pass the Objective to the scrapper tool, and the URL to be extracted
        - the objective the user provide is the fileds that you want to extract .
        User: {input}
        Assistant:
        """
    from Packages.Agents.Init_Agent import initialize_agent_tool
    agent = initialize_agent_tool()
    response = agent.init_agent(tools,prompt,objective,AgentType.STRUCTURED_CHAT_ZERO_SHOT_REACT_DESCRIPTION)
    return (response)
 
class ScrapperAgentToolInput(BaseModel):
    """Inputs for Scrapper_Agent"""

    objective: str = Field(description="the objective required for agent prompt input")

  
class ScrapperAgentTool(BaseTool):
    name = "Scrapper_Agent"
    description = """
        The Scrapper Apps Toolkit is designed for extracting data(fileds) from any website by provideing the fileds and URL for the website.
        Use this agent when user need to scrap or extract any data from given URL .
        """
    args_schema: Type[BaseModel] = ScrapperAgentToolInput
    def get_description(self):
        return self.description
    
    def _run(self, objective: str) -> str:
        print("\n Scrapper_Agent: " +objective)
        response = Scrapper_Agent(objective)
        return response

    def _arun(self, objective: str):
        raise NotImplementedError("Scrapper_Agent does not support async")