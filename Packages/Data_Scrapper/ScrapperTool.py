import json
import openai
from langchain.document_loaders import AsyncChromiumLoader
from langchain.document_transformers import BeautifulSoupTransformer
from langchain.prompts import PromptTemplate
import os
from langchain.tools import BaseTool 
from pydantic import BaseModel, Field
import pandas as pd
from typing import Optional, Type
import io
import pandas as pd
import io
from openpyxl import Workbook
from openpyxl.utils.dataframe import dataframe_to_rows
from googleapiclient.discovery import build
from googleapiclient.http import MediaFileUpload
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
import pickle
import os

openai.api_key = "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"
def fetch_and_process_data(url):
    """
    Fetches and processes data from a given URL using web scraping.

    Args:
        url (str): The URL of the website to scrape.

    Returns:
        str: A JSON-formatted string containing the extracted data.
    """
    loader = AsyncChromiumLoader([url])
    html = loader.load()
    bs_transformer = BeautifulSoupTransformer()
    docs_transformed = bs_transformer.transform_documents(html)
    extracted_data = []
    for doc in docs_transformed:
        extracted_data.append({
            "page_content": doc.page_content
        })
    json_result = json.dumps(extracted_data)
    return json_result

def send_to_llm(context_json, query):
    """
    Sends a query to the language model and receives a response.

    Args:
        context_json (str): JSON-formatted context for the language model.
        query (str): The user's query.

    Returns:
        str: The response from the language model.
    """
    context_str = json.dumps(context_json)
    chunk_size = 8000
    chunks = [context_str[i:i+chunk_size] for i in range(0, len(context_str), chunk_size)]
    responses = []
    for chunk in chunks:
        messages = [
            {"role": "system", "content": "You are a helpful assistant."},
            {"role": "user", "content": f"The following is a context: {chunk}"},
            {"role": "user", "content": query}
        ]
        response = openai.ChatCompletion.create(
            model="gpt-3.5-turbo-16k",
            messages=messages
        )
        responses.append(response.choices[0].message['content'].strip())
    return '\n'.join(responses)

def scrap_data(url, question):
    """
    Extracts(Scrap) fileds from a given URL by sending a query to the language model.



    Args:
        url (str): The URL of the website to extract information from.
        question (str): The user's question or query.

    Returns:
        str: The extracted information as a formatted response.
    """
    context_json = fetch_and_process_data(url)
    TEMPLATE = """
    Given the following question:
    Question: {question}
    Given the following URL:
    URL: {url}
    Please extract just the filed that user ask.
    please Make sure to not exceed the limit of the token 

    Format the extracted data in a table:
    """
    prompt_template = PromptTemplate(
        input_variables=["question", "url"],
        template=TEMPLATE,
    )
    formatted_prompt = prompt_template.format(question=question, url=url)
    answer = send_to_llm(context_json, formatted_prompt)
    answer_to_excel(answer)
    return answer
    
def answer_to_excel(answer: str, output_filename: str = "output.xlsx"):
    """
    Converts the answer string to an Excel file.

    Args:
        answer (str): The extracted answer as a string.
        output_filename (str): The name of the Excel file to save.
    """
    try:
        # Convert the answer to DataFrame
        df = pd.read_csv(io.StringIO(answer), sep="|", on_bad_lines='skip')

        # Clean up column names and remove leading/trailing spaces
        df.columns = [col.strip() for col in df.columns]

        # Remove the "Unnamed" column
        df = df.loc[:, ~df.columns.str.contains('^Unnamed')]

        # Remove leading/trailing spaces in values
        df = df.apply(lambda col: col.str.strip() if col.dtype == 'object' else col)

        # Remove lines containing "-------"
        df = df[~df.astype(str).apply(lambda row: row.str.contains('-').any(), axis=1)]
        # Convert the DataFrame to an Excel file
        wb = Workbook()
        ws = wb.active

        for r_idx, row in enumerate(dataframe_to_rows(df, index=False, header=True), 1):
            for c_idx, value in enumerate(row, 1):
                ws.cell(row=r_idx, column=c_idx, value=value)

        # Adjust columns width
        for column in ws.columns:
            max_length = 0
            column = [cell for cell in column]
            for cell in column:
                try: 
                    if len(str(cell.value)) > max_length:
                        max_length = len(cell.value)
                except:
                    pass
            adjusted_width = (max_length + 2)
            ws.column_dimensions[column[0].column_letter].width = adjusted_width

        # Save the file
        file=wb.save(output_filename)
        try:
            upload_file_to_google_drive(output_filename,'https://drive.google.com/drive/folders/1HuTAXjkSP8eWnSol7qvD7BQ1HAkybeSR')
        except Exception as e:
            print("error",e)
        
        return answer 

    except Exception as e:
        # Return a failure message with the error
        return f"An error occurred while creating the Excel: {e}"
def get_folder_id_from_url(folder_url):
    """Extract the folder ID from a Google Drive folder URL."""
    # Assuming the URL is of the form: https://drive.google.com/drive/folders/{folder_id}
    return folder_url.split('/')[-1]

def upload_file_to_google_drive(filename, folder_url):
    """Uploads a file to a specified Google Drive folder by folder URL."""
    # If modifying these scopes, delete the file token.pickle.
    SCOPES = ['https://www.googleapis.com/auth/drive']

    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first time.
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file('Packages\Data_Scrapper\Cred5.json', SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    service = build('drive', 'v3', credentials=creds)

    folder_id = get_folder_id_from_url(folder_url)
    if folder_id is None:
        print(f"Folder URL '{folder_url}' is invalid.")
        return

    file_metadata = {'name': os.path.basename(filename), 'parents': [folder_id]}
    media = MediaFileUpload(filename, resumable=True)
    file = service.files().create(body=file_metadata, media_body=media, fields='id').execute()
    print(f"File ID: {file.get('id')}")
# ToolKit
# ==================================================================>
class ScrapperInputs(BaseModel):
    """Inputs for scrap_data"""

    url :str =Field(description="The URL of the website to extract information from.")
    question: str =Field("The user's question or query ")
    


class CreateScrapperTool(BaseTool):
    name = "scrap_data"
    description = """
    This function is used to scrape data from a given URL by leveraging the capabilities of a language model. 
    It sends a query to the language model, which then processes the provided URL and retrieves the desired information. 
    The function takes advantage of the language model's ability to understand and extract relevant data from webpages. 
    Thanks to the natural language processing capabilities of the language model, it can efficiently parse and structure the desired information. 
    This results in an effective and precise extraction of information from the specified URL.
    """
    args_schema: Type[BaseModel] = ScrapperInputs
    def _run(self,url:str, question:str):
        response= scrap_data(url,question)
        return response +'The result saved to the following google drive folder https://drive.google.com/drive/folders/1HuTAXjkSP8eWnSol7qvD7BQ1HAkybeSR'
    
    
    

