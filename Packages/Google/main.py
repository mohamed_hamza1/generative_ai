from dotenv import load_dotenv
from langchain.chat_models import ChatOpenAI
from langchain.agents import AgentType, initialize_agent

from Gmail.tools import (
    DownloadAttachment,
    SendMail,
    ListEmails,
    SearchAndRespond,
    CreateReplyAndSaveAsDraft,
    ForwardMail,
)

from Calendar.tools import (
    SendInvite,
    GetInviteDetails,
    CancelInvites,
    UpdateEvents,
)

from dotenv import load_dotenv

load_dotenv()

tool = [
    DownloadAttachment(),
    SendMail(),
    ListEmails(),
    SearchAndRespond(),
    CreateReplyAndSaveAsDraft(),
    SendInvite(),
    GetInviteDetails(),
    CancelInvites(),
    UpdateEvents(),
    ForwardMail(),
]

llm = ChatOpenAI(model="gpt-3.5-turbo-0613", temperature=0)

agent = initialize_agent(
    tools=tool, llm=llm, agent=AgentType.OPENAI_FUNCTIONS, verbose=True
)

# agent.run(
#     "bhavini.chouhan79@gmail.com sent you an email on October 10, 2023 with the subject: New Email List_10 Oct 2023 and Exclusion 10th Oct. Please download the attachment to that email."
# )

# agent.run(
#     "Send a mail to vikas.sanwal@oodles.io and priya.sharma@oodles.io about Neural demo. Please tell them to be available at 3:00 PM IST and tell them how important this product is for our company and it is a revolutionary product and add some stuff about Generative AI. PS: Thanks by Jizo Op - Rocket Scientist. Keep jizo.op@oodles.io in bcc and vikas.yadav@oodles.io in cc"
# )


# agent.run("List the emails that I've received from chayan@neuralwave.ai and set the max results to 5")

# agent.run("Please respond to this email team@mail.notion.so with subject Notion Affiliates is now global! that I'll look into it and thank you for suggesting me this great idea. The message type is read as I've already opened it")

# agent.run(
#     "Please create a reply to this email deborah@testsigmainc.com with the subject October digest - All about mobile-first testing and save it to draft. Tell them it is a phenomenal idea and we are please to connect with you and tell something about How this product is going to be important for us. The message type is read as I've already opened it. Best regards - Jizo"
# )

# agent.run(
#     "Please send an invite on calendar. The summary is Blood donation camp and the attendees will be jizo@oodles.io. Location will be White house. The start time will be 3:00 PM IST oct 25th 2023 and will take around 30 minutes. The description will be that All employees of Oodles need to be available  at this camp and need to donate at least 3 litres of blood avoiding this will make them compliant to donate their right kidney"
# )

# agent.run("Please provide me the details of the Kidney donation camp event")


# agent.run("Please cancel the event with id n25kseqikm387l7u3h0ot8vfg0")

# agent.run(
#     "Please update the invite on calendar. The event id is 7dtr975egc798gm6un80e429i8. The summary needs to be changed to Kidney donation camp and two attendees will be jiji@oodlez.io and dhruv.gupta@oodles.io. Location will be Iris tech park, Gurugram sector - 48. The start time will be 10:00 AM IST. The description will be that All employees of Oodles need to be available  at this camp and need to donate their right kidney. Failing to do so, will make them compliant to donate all their life savings. The start time will be 2:00 PM IST oct 28th 2023 and will take around 30 minutes. "
# )



# agent.run(
#     "Forward the mail to druvi@oodlez.io and jackie@oodle.io that I've received from vishnupriya@codedamn.com. Please tell to enroll this course and the cost incurred will be compensated by Preeti Choudhary. The type of the message is read"
# )

agent.run("Create a label with name test")