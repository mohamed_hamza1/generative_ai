import json
import requests
from typing import Type
from dotenv import load_dotenv
from pydantic import BaseModel, Field
from langchain.tools import BaseTool

load_dotenv()


# Send mail
def send_mail(email: list, cc: list, bcc: list, subject: str, body: str) -> json:
    """Method to send mail using gmail"""

    url = "http://localhost:3000/gmail/send-mail"
    data = {"email": email, "cc": cc, "bcc": bcc, "subject": subject, "body": body}
    headers = {"Content-Type": "application/json", "Accept": "application/json"}
    response = requests.post(url, headers=headers, json=data)
    if response.status_code == 200:
        response_data = response.json()
        return response_data
    else:
        return {"error": f"Request failed with status code {response.status_code}"}


class SendMailSchema(BaseModel):
    email: list = Field(
        description="The email address of the primary recipient of the email. This is the person to whom email will be sent. This field is mandatory while sending the email. At least one email address must be provided",
    )
    cc: list = Field(
        description="The email addresses of the secondary recipients of the email. These people will also receive the email in their inbox, but they will know that they are not the primary recipient. This field is not mandatory while sending the email."
    )
    bcc: list = Field(
        description="The email addresses of the blind carbon copy recipients of the email. These people will receive the email in their inbox, but the primary recipient will not know that they have been copied. This field is not mandatory while sending the email."
    )
    subject: str = Field(
        description="The subject line of the email. This is the first line of the email that the recipient will see, so it is important to make it clear and concise. This field is mandatory while sending the email."
    )
    body: str = Field(
        description="The body of the email. This is the main content of the email. You can use this field to write anything you want, but it is important to be polite and professional."
    )


class SendMail(BaseTool):
    name = "send_mail"
    description = "The SendMail class can be used to send emails to specific email addresses with a specific subject line and body using your gmail account. You can also provide additional information, such as CC and BCC recipients, and attachments."
    args_schema: Type[SendMailSchema] = SendMailSchema

    def _run(self, email: list, cc: list, bcc: list, subject: str, body: str) -> json:
        response = send_mail(email, cc, bcc, subject, body)
        return response

    async def _arun(
        self, email: list, cc: list, bcc: list, subject: str, body: str
    ) -> json:
        raise NotImplementedError("Not supported")


# List emails
def list_emails(
    max_results: int,
    message_type: str,
    email_from: str,
    label: str,
    subject: str,
) -> json:
    """Method to list mail in your gmail inbox"""
    url = "http://localhost:3000/gmail/list-emails"
    data = {
        "max_results": max_results,
        "message_type": message_type,
        "email_from": email_from,
        "label": label,
        "subject": subject,
    }
    headers = {"Content-Type": "application/json", "Accept": "application/json"}
    response = requests.post(url, headers=headers, json=data)
    if response.status_code == 200:
        response_data = response.json()
        return response_data
    else:
        return {"error": f"Request failed with status code {response.status_code}"}


class ListEmailsSchema(BaseModel):
    max_results: int = Field(
        description="max_results is the number of emails to be displayed at once. For example, if you set max_results to 10, then only the 10 most recent emails will be displayed."
    )
    message_type: str = Field(
        description="message_type is the type of email to be displayed. You can choose to display only read emails, unread emails, or all emails."
    )
    email_from: str = Field(
        description="email_from is the email address of the person who sent the email. You can choose to display only emails from a specific person, or emails from all people"
    )
    label: str = Field(
        description="label is the label of the email. You can choose to display only emails with a specific label, or emails with all labels. If the label is not provided then by default it is set as Inbox"
    )
    subject: str = Field(
        description="subject is the subject line of the email. You can choose to display only emails with a specific subject line, or emails with all subject lines."
    )


class ListEmails(BaseTool):
    name = "list_emails"
    description = "The ListEmails tool allows you to filter and display emails based on a variety of criteria in your gmail inbox. This can be useful for keeping track of important emails, finding emails that you need to respond to, or organizing your emails. Simply specify the desired criteria, such as the number of emails to display, the type of email to display, the person who sent the email, the label of the email, or the subject line of the email. The tool will then return a list of emails that match the criteria."
    args_schema: Type[ListEmailsSchema] = ListEmailsSchema

    def _run(
        self,
        max_results: int,
        message_type: str,
        email_from: str,
        label: str,
        subject: str,
    ) -> json:
        response = list_emails(max_results, message_type, email_from, label, subject)
        return response

    async def _arun(
        self,
        max_results: int,
        message_type: str,
        email_from: str,
        label: str,
        subject: str,
    ) -> str:
        raise NotImplementedError("Not supported")


# Download attachment
def download_attachment(email: str, subject: str) -> json:
    """Method to attachemnets that has been attached to your email in gmail inbox"""

    url = "http://localhost:3000/gmail/download-attachments"
    data = {"email": email, "subject": subject}
    headers = {"Content-Type": "application/json", "Accept": "application/json"}
    response = requests.post(url, headers=headers, json=data)
    if response.status_code == 200:
        response_data = response.json()
        return response_data
    else:
        return {"error": f"Request failed with status code {response.status_code}"}


class DownloadAttachmentSchema(BaseModel):
    email: str = Field(
        description="The email address to search for. This is the email address that the attachment was sent to."
    )
    subject: str = Field(
        description="The subject line of the email to search for. This is the subject line of the email that contains the attachment."
    )


class DownloadAttachment(BaseTool):
    name = "download_attachment"
    description = "This tool downloads the attachment from the email that was sent to the specified email address and has the specified subject line in your gmail inbox. The attachment will be downloaded to the current working directory."
    args_schema: Type[DownloadAttachmentSchema] = DownloadAttachmentSchema

    def _run(self, email: str, subject: str) -> json:
        response = download_attachment(email, subject)
        return response

    async def _arun(self, email: str, subject: str) -> str:
        raise NotImplementedError("Not supported")


# Respond to emails
def search_and_respond(
    message_type: str,
    email_from: str,
    label: str,
    subject: str,
    messageBody: str,
) -> json:
    """Method to search and respond as a draft in you gmail account"""
    url = "http://localhost:3000/gmail/search-and-respond"
    data = {
        "message_type": message_type,
        "email_from": email_from,
        "label": label,
        "subject": subject,
        "messageBody": messageBody,
    }
    headers = {"Content-Type": "application/json", "Accept": "application/json"}
    response = requests.post(url, headers=headers, json=data)
    if response.status_code == 200:
        response_data = response.json()
        return response_data
    else:
        return {"error": f"Request failed with status code {response.status_code}"}


class SearchAndRespondSchema(BaseModel):
    message_type: str = Field(
        description="message_type is the type of email to be displayed. By default message_type will be read if not specified explicitly but the user can choose read emails, unread emails, or all emails.",
        default="read",
    )
    email_from: str = Field(
        description="email_from is the email address of the person who sent you the email. You can choose the emails from a specific person, or emails from all people"
    )
    label: str = Field(
        description="label is the label of the email. You can choose the emails with a specific label, or emails with all labels. If the label is not provided then by default it is set as Inbox"
    )
    subject: str = Field(
        description="subject is the subject line of the email. You can choose the emails with a specific subject line, or emails with all subject lines."
    )
    messageBody: str = Field(description="The body of the email to be sent.")


class SearchAndRespond(BaseTool):
    name = "search_and_respond"
    description = "The SearchAndRespond tool is designed to send responses as adraft to specific email addresses based on certain criteria, including message type, email sender, label, subject, and message body. Whether you need to reply to important messages or enhance your email organization, this tool has you covered. Simply define your desired criteria, and the tool will present you with the most pertinent email details based on your criteria, enabling you to promptly respond to the topmost result or the latest email that meets your specified conditions. If the label is not provided then by default Inbox should be provided"
    args_schema: Type[SearchAndRespondSchema] = SearchAndRespondSchema

    def _run(
        self,
        message_type: str,
        email_from: str,
        label: str,
        subject: str,
        messageBody: str,
    ) -> json:
        response = search_and_respond(
            message_type, email_from, label, subject, messageBody
        )
        return response

    async def _arun(
        self,
        message_type: str,
        email_from: str,
        label: str,
        subject: str,
        messageBody: str,
    ) -> str:
        raise NotImplementedError("Not supported")


# Create a reply and save as draft
def create_reply_and_save_as_draft(
    message_type: str,
    email_from: str,
    label: str,
    subject: str,
    messageBody: str,
) -> json:
    
    """Method to create replay and save it as a draft using your gmail account"""
    
    url = "http://localhost:3000/gmail/create-reply-and-draft"
    data = {
        "message_type": message_type,
        "email_from": email_from,
        "label": label,
        "subject": subject,
        "messageBody": messageBody,
    }
    headers = {"Content-Type": "application/json", "Accept": "application/json"}
    response = requests.post(url, headers=headers, json=data)
    if response.status_code == 200:
        response_data = response.json()
        return response_data
    else:
        return {"error": f"Request failed with status code {response.status_code}"}


class CreateReplyAndSaveAsDraftSchema(BaseModel):
    message_type: str = Field(
        description="message_type is the type of email to be displayed. By default message_type will be read if not specified explicitly but the user can choose read emails, unread emails, or all emails."
    )
    email_from: str = Field(
        description="email_from is the email address of the person who sent you the email. You can choose the emails from a specific person, or emails from all people"
    )
    label: str = Field(
        description="label is the label of the email. You can choose the emails with a specific label, or emails with all labels. If the label is not provided then by default it is set as Inbox"
    )
    subject: str = Field(
        description="subject is the subject line of the email. You can choose the emails with a specific subject line, or emails with all subject lines."
    )
    messageBody: str = Field(description="The body of the email to be sent.")


class CreateReplyAndSaveAsDraft(BaseTool):
    name = "create_reply_and_save_as_draft"
    description = "The CreateReplyAndSaveAsDraft tool is a powerful utility designed to streamline your gmail email management process by creating draft responses and, if necessary, sending them to specific email addresses. This tool offers an efficient way to draft responses based on various criteria, such as message type, email sender, label, subject, and message content."
    args_schema: Type[CreateReplyAndSaveAsDraftSchema] = CreateReplyAndSaveAsDraftSchema

    def _run(
        self,
        message_type: str,
        email_from: str,
        label: str,
        subject: str,
        messageBody: str,
    ) -> json:
        response = create_reply_and_save_as_draft(
            message_type, email_from, label, subject, messageBody
        )
        return response

    async def _arun(
        self,
        message_type: str,
        email_from: str,
        label: str,
        subject: str,
        messageBody: str,
    ) -> str:
        raise NotImplementedError("Not supported")


# Forward mail
def forward_mail(
    recipients: list,
    message_type: str,
    email_from: str,
    label: str,
    subject: str,
    messageBody: str,
) -> json:
    
    """Method to foward an email based on a certain criteria using your gmail account"""
    
    url = "http://localhost:3000/gmail/forward"
    data = {
        "recipients": recipients,
        "message_type": message_type,
        "email_from": email_from,
        "label": label,
        "subject": subject,
        "messageBody": messageBody,
    }
    headers = {"Content-Type": "application/json", "Accept": "application/json"}
    response = requests.post(url, headers=headers, json=data)
    if response.status_code == 200:
        response_data = response.json()
        return response_data
    else:
        return {"error": f"Request failed with status code {response.status_code}"}


class ForwardMailSchema(BaseModel):
    recipients: list = Field(
        description="A list of email addresses to which the email should be forwarded. At least one email address must be provided.",
    )

    message_type: str = Field(
        description="The type of email to be forwarded. Valid values are `READ`, `UNREAD`, or `ALL`. Defaults to `ALL`.",
    )

    email_from: str = Field(
        description="The email address of the original sender of the email.",
    )

    label: str = Field(
        description="The label of the email, if any.",
    )

    subject: str = Field(
        description="The subject line of the email.",
    )

    messageBody: str = Field(
        description="The body of the email.",
    )


class ForwardMail(BaseTool):
    name = "forward_mail"
    description = "Forwards an email to a list of recipients using your gmail account. The email can be filtered by read status, unread status, or all emails. The email body can also be optionally included in the forwarded email."
    args_schema: Type[ForwardMailSchema] = ForwardMailSchema

    def _run(
        self,
        recipients: list,
        message_type: str,
        email_from: str,
        label: str,
        subject: str,
        messageBody: str,
    ) -> json:
        response = forward_mail(
            recipients, message_type, email_from, label, subject, messageBody
        )
        return response

    async def _arun(
        self,
        recipients: list,
        message_type: str,
        email_from: str,
        label: str,
        subject: str,
        messageBody: str,
    ) -> json:
        raise NotImplementedError("Not supported")


# Move email to a label/ folder
def move_email(email_id: str, label: str) -> json:

    """Method to move an email in you gmail inbox from one label to another"""
    
    url = "http://localhost:3000/gmail/move-email"
    data = {"email_id": email_id, "label": label}
    headers = {"Content-Type": "application/json", "Accept": "application/json"}
    response = requests.post(url, headers=headers, json=data)
    if response.status_code == 200:
        response_data = response.json()
        return response_data
    else:
        return {"error": f"Request failed with status code {response.status_code}"}


class MoveEmailSchema(BaseModel):
    email_id: str = Field(
        description="This is the email ID of the email to which you want to add a label",
    )

    label: str = Field(
        description="This is the name of the label to which you want to add the email",
    )


class MoveEmail(BaseTool):
    name = "move_email"
    description = "This function moves an email to a particular Label so that the user can categorize the emails based on their choice using your gmail account"
    args_schema: Type[MoveEmailSchema] = MoveEmailSchema

    def _run(self, email_id: str, label: str) -> json:
        response = move_email(email_id, label)
        return response

    async def _arun(self, email_id: str, label: str) -> json:
        raise NotImplementedError("Not supported")


# Create label/ folder
def create_label(label_name: str) -> json:

    """Method to create label in your gmail inbox"""

    url = "http://localhost:3000/gmail/create-label"
    data = {"label_name": label_name}
    headers = {"Content-Type": "application/json", "Accept": "application/json"}
    response = requests.post(url, headers=headers, json=data)
    if response.status_code == 200:
        response_data = response.json()
        return response_data
    else:
        return {"error": f"Request failed with status code {response.status_code}"}


class CreateLabelSchema(BaseModel):
    label_name: str = Field(
        description="This is the name of the label which you want to create",
    )


class CreateLabel(BaseTool):
    name = "create_label"
    description = "This function is used to create the label in Gmail."
    args_schema: Type[CreateLabelSchema] = CreateLabelSchema

    def _run(self, label_name: str) -> json:
        response = create_label(label_name)
        return response

    async def _arun(self, label_name: str) -> json:
        raise NotImplementedError("Not supported")