from typing import Type
from pydantic import BaseModel, Field
from langchain.tools import BaseTool
from langchain.agents import AgentType,Tool,initialize_agent
from langchain.chat_models import ChatOpenAI
from dotenv import load_dotenv

from Packages.Google.Gmail.tools import (
    DownloadAttachment,
    SendMail,
    ListEmails,
    SearchAndRespond,
    CreateReplyAndSaveAsDraft,
    ForwardMail,
)

from Packages.Google.Calendar.tools import (
    SendInvite,
    GetInviteDetails,
    CancelInvites,
    UpdateEvents,
)

load_dotenv()

def Google_Agent(objective):
    """Method to call Google agent toolkit"""
    tools = [
    DownloadAttachment(),
    SendMail(),
    ListEmails(),
    SearchAndRespond(),
    CreateReplyAndSaveAsDraft(),
    SendInvite(),
    GetInviteDetails(),
    CancelInvites(),
    UpdateEvents(),
    ForwardMail(),]
    prompt = """
    You are an assistant that helps to get access to Google tools including Gmail, and Calender. You should always:
    - Take the user input, and list all the provided arguments within the user input.
    - Pass the arguments to the provided tools when being used
    - Be polite and helpful
    - Download Attachment using the provided tools when asked
    - Send an Email using the provided tools when asked
    - List Emails using the provided tools when asked
    - Search for and Email and respond using the provided tools when asked
    - Create Reply and save it as Draft using the provided tools when asked
    - Send invite using google calender sendinvite tool using the provided tools when asked
    - Get Invite details from google calender GetInviteDetails tool using the provided tools when asked
    - Cancel Invites from google calender CancelInvites tool using the provided tools when asked
    - Update Events details from google UpdateEvents tool using the provided tools when asked
    - Forward Email using google gmail ForwardMail() tool using the provided tools when asked
    - Provide explanations for your actions 
    User: {input}
    Assistant:
    """
    from Packages.Agents.Init_Agent import initialize_agent_tool
    agent = initialize_agent_tool()
    response = agent.init_agent(tools,prompt,objective,AgentType.STRUCTURED_CHAT_ZERO_SHOT_REACT_DESCRIPTION)

    return (response)
 
class GoogleAgentToolInput(BaseModel):
    """Inputs for Google_Agent"""

    objective: str = Field(description="the objective required for agent prompt input")

  
class GoogleAgentTool(BaseTool):
    name = "Google_Agent"
    description = """
        Helpful when you wish to set up the agent for Google Apps Toolkit, which utilizes tools related to Gmail and the calendar features of Google Apps.
        """
    args_schema: Type[BaseModel] = GoogleAgentToolInput
    def get_description(self):
        return self.description
    def _run(self, objective: str) -> str:
        response = Google_Agent(objective)
        return response

    def _arun(self, objective: str):
        raise NotImplementedError("Google_Agent does not support async")