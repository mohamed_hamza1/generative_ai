import json
import requests
from typing import Type
from dotenv import load_dotenv
from pydantic import BaseModel, Field
from langchain.tools import BaseTool

load_dotenv()


# Send Invites
def send_invite(
    summary: str,
    description: str,
    location: str,
    start_time: str,
    end_time: str,
    timeZone: str,
    attendees: list,
) -> json:
    url = "http://localhost:3000/google-calendar/create-event"

    formatted_attendees = [{"email": email} for email in attendees]

    data = {
        "summary": summary,
        "description": description,
        "location": location,
        "start_time": start_time,
        "end_time": end_time,
        "timeZone": timeZone,
        "attendees": attendees,
    }
    headers = {"Content-Type": "application/json", "Accept": "application/json"}
    response = requests.post(url, headers=headers, json=data)
    if response.status_code == 200:
        response_data = response.json()
        return response_data
    else:
        return {"error": f"Request failed with status code {response.status_code}"}


class SendInviteSchema(BaseModel):
    summary: str = Field(description="The title of the event.")
    location: str = Field(
        description="The location of the event. This can be a physical address, a URL, or any other text that describes where the event is taking place."
    )
    description: str = Field(
        description="The location of the event. This can be a physical address, a URL, or any other text that describes where the event is taking place."
    )
    start_time: str = Field(
        description="The start time of the event. This should be in ISO 8601 format, with the appropriate UTC offset."
    )
    end_time: str = Field(
        description="The end time of the event. This should be in ISO 8601 format, with the appropriate UTC offset."
    )
    timeZone: str = Field(
        description="The time zone of the event. This should be in IANA Time Zone Database (TZDB) format."
    )
    attendees: list = Field(
        description="A list of email addresses of the people who are being invited to the event."
    )


class SendInvite(BaseTool):
    name = "send_invite"
    description = "This tool is designed to send event invitations. It takes various details about the event, such as the title, location, start and end times, time zone, and a list of attendees' email addresses, and generates an invitation message. The event details are structured using the SendInviteSchema, which includes a summary of the event, its location, start and end times, time zone, and the list of attendees. You can use this tool to automate the process of sending event invitations."
    args_schema: Type[SendInviteSchema] = SendInviteSchema

    def _run(
        self,
        summary: str,
        description: str,
        location: str,
        start_time: str,
        end_time: str,
        timeZone: str,
        attendees: list,
    ) -> json:
        response = send_invite(
            summary, description, location, start_time, end_time, timeZone, attendees
        )
        return response

    async def _arun(
        self,
        summary: str,
        description: str,
        location: str,
        start_time: str,
        end_time: str,
        timeZone: str,
        attendees: list,
    ) -> str:
        raise NotImplementedError("Not supported")


# List invites
def get_invite_details(
    title: str,
) -> json:
    url = "http://localhost:3000/google-calendar/get-event-details"

    data = {
        "title": title,
    }
    headers = {"Content-Type": "application/json", "Accept": "application/json"}
    response = requests.post(url, headers=headers, json=data)
    if response.status_code == 200:
        response_data = response.json()
        return response_data
    else:
        return {"error": f"Request failed with status code {response.status_code}"}


class GetInviteDetailsSchema(BaseModel):
    title: str = Field(
        description="The name or title associated with the event on your calendar. You can search for events using this title."
    )


class GetInviteDetails(BaseTool):
    name = "get_invite_details"
    description = "Retrieve event details using the provided title. This tool fetches information about an event, such as its location, start and end times, time zone, and attendee list based on the event's title."
    args_schema: Type[GetInviteDetailsSchema] = GetInviteDetailsSchema

    def _run(
        self,
        title: str,
    ) -> json:
        response = get_invite_details(title)
        return response

    async def _arun(
        self,
        title: str,
    ) -> str:
        raise NotImplementedError("Not supported")


# Cancel invites
def cancel_events(
    id: str,
) -> json:
    url = "http://localhost:3000/google-calendar/cancel-event"

    data = {
        "id": id,
    }
    headers = {"Content-Type": "application/json", "Accept": "application/json"}
    response = requests.post(url, headers=headers, json=data)
    if response.status_code == 200:
        response_data = response.json()
        return response_data
    else:
        return {"error": f"Request failed with status code {response.status_code}"}


class CancelInvitesSchema(BaseModel):
    id: str = Field(
        description="The name or id associated with the event on your calendar. You can search for events using this id."
    )


class CancelInvites(BaseTool):
    name = "cancel_events"
    description = "Cancel event using the provided id. This tool cancels the events based on the event's id."
    args_schema: Type[CancelInvitesSchema] = CancelInvitesSchema

    def _run(
        self,
        id: str,
    ) -> json:
        response = cancel_events(id)
        return response

    async def _arun(
        self,
        id: str,
    ) -> str:
        raise NotImplementedError("Not supported")


# Update Invites
def update_event(
    id: str,
    summary: str,
    description: str,
    location: str,
    start_time: str,
    end_time: str,
    timeZone: str,
    attendees: list,
) -> json:
    url = "http://localhost:3000/google-calendar/update-event"

    # formatted_attendees = [{"email": email} for email in attendees]

    data = {
        "id": id,
        "summary": summary,
        "description": description,
        "location": location,
        "start_time": start_time,
        "end_time": end_time,
        "timeZone": timeZone,
        "attendees": attendees,
    }
    headers = {"Content-Type": "application/json", "Accept": "application/json"}
    response = requests.post(url, headers=headers, json=data)
    if response.status_code == 200:
        response_data = response.json()
        return response_data
    else:
        return {"error": f"Request failed with status code {response.status_code}"}


class UpdateEventsSchema(BaseModel):
    id: str = Field(
        description="The name or id associated with the event on your calendar. You can search for events using this id."
    )
    summary: str = Field(description="The title of the event.")
    location: str = Field(
        description="The location of the event. This can be a physical address, a URL, or any other text that describes where the event is taking place."
    )
    description: str = Field(
        description="The location of the event. This can be a physical address, a URL, or any other text that describes where the event is taking place."
    )
    start_time: str = Field(
        description="The start time of the event. This should be in ISO 8601 format, with the appropriate UTC offset.",
        default="",
    )
    end_time: str = Field(
        description="The end time of the event. This should be in ISO 8601 format, with the appropriate UTC offset.",
        default="",
    )
    timeZone: str = Field(
        description="The time zone of the event. This should be in IANA Time Zone Database (TZDB) format."
    )
    attendees: list = Field(
        description="A list of email addresses of the people who are being invited to the event."
    )


class UpdateEvents(BaseTool):
    name = "update_event"
    description = "This tool is designed to update events. It takes various details about the event, such as the title, location, start and end times, time zone, and a list of attendees' email addresses, and generates an updates the event accordingly."
    args_schema: Type[UpdateEventsSchema] = UpdateEventsSchema

    def _run(
        self,
        id: str,
        summary: str,
        description: str,
        location: str,
        start_time: str,
        end_time: str,
        timeZone: str,
        attendees: list,
    ) -> json:
        response = update_event(
            id,
            summary,
            description,
            location,
            start_time,
            end_time,
            timeZone,
            attendees,
        )
        return response

    async def _arun(
        self,
        id: str,
        summary: str,
        description: str,
        location: str,
        start_time: str,
        end_time: str,
        timeZone: str,
        attendees: list,
    ) -> str:
        raise NotImplementedError("Not supported")
