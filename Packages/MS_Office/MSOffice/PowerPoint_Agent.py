from typing import Type
from pydantic import BaseModel, Field
from langchain.tools import BaseTool
from langchain.agents import AgentType,Tool,initialize_agent
from langchain.chat_models import ChatOpenAI
from Packages.MS_Office.MSOffice.PowerPoint import createPowePointTool
from dotenv import load_dotenv
load_dotenv()

def PowerPoint_Agent(objective):
    """Method to call Power Point agent toolkit"""
    tools = [createPowePointTool()]
    prompt = """
        You are an assistant that helps create a Power Point presntation file . You should always:
        - Be polite and helpful
        - Create Power Point tool using the provided tools when asked
        - Create PowerPoint file through Microsoft createPowePointTool using the provided tools when asked 
        - Provide explanations for your actions
        - When ready to generate the Power point prenstation, provide a detailed description of the content you will include, organized into sections and subsections, if applicable. Ensure the document you describe is well-structured and meets the user's specified objective.
        User: {input}
        Assistant:
        """
    from Packages.Agents.Init_Agent import initialize_agent_tool
    agent = initialize_agent_tool()
    response = agent.init_agent(tools,prompt,objective,AgentType.STRUCTURED_CHAT_ZERO_SHOT_REACT_DESCRIPTION)
    return (response)
 
class PowerPointAgentToolInput(BaseModel):
    """Inputs for PowerPoint_Agent"""

    objective: str = Field(description="the objective required for agent prompt input")

  
class PowerPintAgentTool(BaseTool):
    name = "PowerPoint_Agent"
    description = """
        The power Point Toolkit is designed for creating a Power Point presnataion file.
        """
    args_schema: Type[BaseModel] = PowerPointAgentToolInput
    def get_description(self):
        return self.description
    
    def _run(self, objective: str) -> str:
        response = PowerPoint_Agent(objective)
        return response

    def _arun(self, objective: str):
        raise NotImplementedError("PowerPoint_Agent does not support async")