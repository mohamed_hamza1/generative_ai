from typing import Type
from pydantic import BaseModel, Field
from langchain.tools import BaseTool
from Packages.MS_Office.MSOffice.PDF import CreatePDFTool
from langchain.agents import AgentType,Tool,initialize_agent
from dotenv import load_dotenv
load_dotenv()

def PDF_Agent(objective):
    """Method to call PDF agent toolkit"""
    tools = [CreatePDFTool()]
    prompt = """
        Here's the formatted version of your text:
        As an assistant responsible for creating a PDF file, you should always adhere to the following guidelines:
        Be Polite and Helpful: Maintain a courteous and supportive demeanor in all interactions.
        Create PDF Using Provided Tools: Utilize the designated tools to assemble the PDF when requested.
        Add Content as Thoroughly as Possible: Ensure that the PDF contains as much relevant content as feasible.
        Provide Explanations for Your Actions: Offer clear justifications for the steps taken during the creation process.
        Add content as much as you can .
        If no user description is provided, automatically include a relevant description using your knowledge base. 
        User: {input}
        Assistant:
        """
    from Packages.Agents.Init_Agent import initialize_agent_tool
    agent = initialize_agent_tool()
    response = agent.init_agent(tools,prompt,objective,AgentType.OPENAI_FUNCTIONS)
    return (response)
 
class PDFAgentToolInput(BaseModel):
    """Inputs for PDF_Agent"""

    objective: str = Field(description="the objective required for agent prompt input")

  
class PDFAgentTool(BaseTool):
    name = "PDF_Agent"
    description = """
        The PDF Toolkit is designed for creating a PDF file.
        """
    args_schema: Type[BaseModel] = PDFAgentToolInput
    def get_description(self):
        return self.description
    
    def _run(self, objective: str) -> str:
        response = PDF_Agent(objective)
        return response

    def _arun(self, objective: str):
        raise NotImplementedError("PDF_Agent does not support async")