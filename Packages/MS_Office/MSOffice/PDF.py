from reportlab.lib.pagesizes import letter
from reportlab.pdfgen import canvas
from reportlab.lib import colors
from langchain.tools import BaseTool 
from pydantic import BaseModel, Field
from typing import Optional, Type
from fpdf import FPDF
from googleapiclient.discovery import build
from googleapiclient.http import MediaFileUpload
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
import pickle
import os
def create_pdf(filename: str, title: str, content: str):
    """
    Create a PDF document with the specified title and content.

    Parameters:
    - filename (str): The name of the output PDF file.
    - title (str): The title that will appear at the top of the PDF document.
    - content (str): The main content/body of the PDF document.
    """
    try:
        # Font, size, style, and alignment configurations
        title_font, title_size, title_style, title_align = "Arial", 16, 'B', 'C'
        content_font, content_size, content_style, content_align = "Arial", 12, '', 'L'
        
        # Page configurations
        page_orientation, page_format = 'P', 'A4'
        
        # Header and Footer configurations
        

        pdf = FPDF(orientation=page_orientation, format=page_format)
        pdf.add_page()

        # Header
        pdf.set_font(content_font, content_style, content_size)

        # Title
        pdf.set_font(title_font, title_style, title_size)
        pdf.cell(0, 10, txt=title, ln=True, align=title_align)

        # Content
        pdf.set_font(content_font, content_style, content_size)
        pdf.multi_cell(0, 10, txt=content, align=content_align)

        # Footer
        pdf.set_y(-15)
        pdf.set_font(content_font, content_style, content_size)

        pdf.output(filename)
        upload_file_to_google_drive(filename,'https://drive.google.com/drive/folders/1nfvfFBc7ktspWKoptL632bZEu6sPDS3H')
        return "PDF created and saved successfully."

    except Exception as e:
        # Return a failure message with the error
        return f"An error occurred while creating the PDF: {e}"
def get_folder_id_from_url(folder_url):
    """Extract the folder ID from a Google Drive folder URL."""
    # Assuming the URL is of the form: https://drive.google.com/drive/folders/{folder_id}
    return folder_url.split('/')[-1]

def upload_file_to_google_drive(filename, folder_url):
    """Uploads a file to a specified Google Drive folder by folder URL."""
    # If modifying these scopes, delete the file token.pickle.
    SCOPES = ['https://www.googleapis.com/auth/drive']

    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first time.
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file('Packages\Data_Scrapper\Cred5.json', SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    service = build('drive', 'v3', credentials=creds)

    folder_id = get_folder_id_from_url(folder_url)
    if folder_id is None:
        print(f"Folder URL '{folder_url}' is invalid.")
        return

    file_metadata = {'name': os.path.basename(filename), 'parents': [folder_id]}
    media = MediaFileUpload(filename, resumable=True)
    file = service.files().create(body=file_metadata, media_body=media, fields='id').execute()
    print(f"File ID: {file.get('id')}")

class PDFInputs(BaseModel):
     """Inputs for create_pdf"""
     filename :str=Field("The name of the output PDF file.") 
     title :str= Field("The title that will appear at the top of the PDF document. ") 
     content :str=Field("The main content/body of the PDF document.")

class CreatePDFTool(BaseTool):
    name = "create_pdf"
    description = """
    This utility is essential for generating PDF files, ensuring a refined and clear presentation. 
    When crafting a document, content clarity and presentation matter a lot, especially when targeting 
    a professional or specific audience. By using this tool, you can streamline the process of creating 
    high-quality PDFs without compromising the document's integrity or layout. Always ensure that the 
    content you input is descriptive and well-structured to achieve the best results with this utility.
    Add content as much as you can .
    If no user description is provided, automatically include a relevant description using your knowledge base.
    """
    args_schema: Type[BaseModel] = PDFInputs
    def _run(self,filename: str, title: str, content: str):
        response=create_pdf(filename,title,content)
        return response +'The created PDF file was saved to the following Google Drive folder: https://drive.google.com/drive/folders/1nfvfFBc7ktspWKoptL632bZEu6sPDS3H'
