import os
from fpdf import FPDF
from typing import Optional, Type
from langchain.tools import BaseTool 
from pydantic import BaseModel, Field
from docx.shared import Pt
from docx.enum.text import WD_ALIGN_PARAGRAPH
from docx import Document
from googleapiclient.discovery import build
from googleapiclient.http import MediaFileUpload
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
import pickle
import os
def create_word_document(title: str, content: list) -> dict:
    """
    Creates a Word document and returns information about the operation.
    """
    try:
        # Ensure the title is file-system friendly
        safe_title = "".join(c for c in title if c.isalnum() or c in " ._-").rstrip()
        file_path = f"{safe_title}.docx"

        doc = Document()

        # Add the main title
        title_paragraph = doc.add_heading(level=1)
        run = title_paragraph.add_run(title)
        run.font.size = Pt(24)  # Set the font size
        title_paragraph.alignment = WD_ALIGN_PARAGRAPH.CENTER  # Center alignment

        # Add sections
        for section in content:
            # Add section title
            doc.add_heading(section['title'], level=2)
            # Add paragraph if it exists
            doc.add_paragraph(section['paragraph'])

        # Save the document to a specified path
        doc.save(file_path)
        upload_file_to_google_drive(file_path,'https://drive.google.com/drive/folders/1jLmxBAHqmxr4ENgd6VMIzP-A1krIoT5t')
        return {'status': 'success', 'file_path': file_path}
        
    except Exception as e:
        return {'status': 'error', 'message': str(e)}
def get_folder_id_from_url(folder_url):
    """Extract the folder ID from a Google Drive folder URL."""
    # Assuming the URL is of the form: https://drive.google.com/drive/folders/{folder_id}
    return folder_url.split('/')[-1]

def upload_file_to_google_drive(filename, folder_url):
    """Uploads a file to a specified Google Drive folder by folder URL."""
    # If modifying these scopes, delete the file token.pickle.
    SCOPES = ['https://www.googleapis.com/auth/drive']

    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first time.
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file('Packages\Data_Scrapper\Cred5.json', SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    service = build('drive', 'v3', credentials=creds)

    folder_id = get_folder_id_from_url(folder_url)
    if folder_id is None:
        print(f"Folder URL '{folder_url}' is invalid.")
        return

    file_metadata = {'name': os.path.basename(filename), 'parents': [folder_id]}
    media = MediaFileUpload(filename, resumable=True)
    file = service.files().create(body=file_metadata, media_body=media, fields='id').execute()
    print(f"File ID: {file.get('id')}")
class WordInputs(BaseModel):
    """Inputs for create_word_document"""

    title :str =Field(description="title of the document.")
    content :list=Field(description="the content of the word file'")


class createWordTool(BaseTool):

    name = "create_word_document"
    description = """
    Create a Word Document that features a prominent title followed by a relevant subtitle. 
    and please return data as dictionarie with title and content and every section in the content list includes a 'paragraph' and 'title' key.
    Each slide within this presentation is essential; it should have a distinct and clear title, making it easy for the audience to follow and understand the narrative. 
    Presenting information in bullet points is recommended, as this format offers clarity and ensures that the key takeaways are concise and to the point.
    Think of each slide as a story - it should be self-contained but also fit seamlessly with the overall narrative. 
    As you design and populate this document, rely on your expertise and knowledge. 
    This isn't just about transcribing facts but weaving them into a compelling story that resonates with and educates the reader.
    """
    args_schema: Type[BaseModel] = WordInputs
    def _run(self,title:str, content:list):
        response=create_word_document(title, content)
        
        return f"{response} The created PDF file was saved to the following Google Drive folder: https://drive.google.com/drive/folders/1jLmxBAHqmxr4ENgd6VMIzP-A1krIoT5t"
