
import os
from typing import Optional, Type
from fpdf import FPDF
from langchain.tools import BaseTool 
from pydantic import BaseModel, Field
from pptx import Presentation
from pptx.enum.shapes import MSO_SHAPE
from pptx.util import Inches
from pptx.enum.text import PP_PARAGRAPH_ALIGNMENT
from pptx.dml.color import RGBColor
import openpyxl
from openpyxl.styles import Font, PatternFill
from googleapiclient.discovery import build
from googleapiclient.http import MediaFileUpload
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
import pickle
import os
def create_excel(data, filename, header_formatting=True):
    """
    Create an Excel file with provided data.
    Please provide a comprehensive template in tabular format suitable for Excel.
      Give the column headings, and data for each row following that. 
      Provide sample data for each row. It should be tabular data, 
      structured in a way that can be used to create an Excel document
      Add content as much as you can .
      If no user description is provided, automatically include a relevant description using your knowledge base.
   
    """
    if os.path.exists(filename):
        print(f"The file '{filename}' already exists. No new file was created.")
        return
    try:
        workbook = openpyxl.Workbook()
        first_sheet = True

        for sheet_name, sheet_data in data.items():
            if first_sheet:
                worksheet = workbook.active
                worksheet.title = sheet_name
                first_sheet = False
            else:
                worksheet = workbook.create_sheet(sheet_name)

            column_widths = []

            for row_idx, row in enumerate(sheet_data, start=1):
                for col_idx, cell_value in enumerate(row, start=1):
                    cell = worksheet.cell(row=row_idx, column=col_idx, value=cell_value)
                    
                    # Save the longest string length for each column
                    try:
                        if len(str(cell_value)) > column_widths[col_idx - 1]:
                            column_widths[col_idx - 1] = len(str(cell_value))
                    except IndexError:
                        column_widths.append(len(str(cell_value)))

                    # If header formatting is enabled and it's the first row
                    if header_formatting and row_idx == 1:
                        cell.font = Font(bold=True, color="FFFFFF")  # White font
                        cell.fill = PatternFill(start_color="000000", end_color="000000", fill_type="solid")  # Black fill
            
            # Set the column width based on the longest string in each column
            for i, column_width in enumerate(column_widths):
                worksheet.column_dimensions[openpyxl.utils.get_column_letter(i+1)].width = column_width

        workbook.save(filename)
        upload_file_to_google_drive(filename,"https://drive.google.com/drive/folders/1hFtP-CaJTO0ILNPZA5rPmZqjiPXHQtHQ")
        return "Excel created and saved successfully."
    except Exception as e:
        # Return a failure message with the error
        return f"An error occurred while creating the Excel: {e}"
    
def get_folder_id_from_url(folder_url):
    """Extract the folder ID from a Google Drive folder URL."""
    # Assuming the URL is of the form: https://drive.google.com/drive/folders/{folder_id}
    return folder_url.split('/')[-1]

def upload_file_to_google_drive(filename, folder_url):
    """Uploads a file to a specified Google Drive folder by folder URL."""
    # If modifying these scopes, delete the file token.pickle.
    SCOPES = ['https://www.googleapis.com/auth/drive']

    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first time.
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file('Packages\Data_Scrapper\Cred5.json', SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    service = build('drive', 'v3', credentials=creds)

    folder_id = get_folder_id_from_url(folder_url)
    if folder_id is None:
        print(f"Folder URL '{folder_url}' is invalid.")
        return

    file_metadata = {'name': os.path.basename(filename), 'parents': [folder_id]}
    media = MediaFileUpload(filename, resumable=True)
    file = service.files().create(body=file_metadata, media_body=media, fields='id').execute()
    print(f"File ID: {file.get('id')}")

class ExcelInputs(BaseModel):
    """Inputs for create_excel"""

    data :dict =Field(description=" A dictionary where key is the sheet name, and value is a list of rows for that sheet")
    filename :str=Field(description="The name of the Excel file to be created")
    # header_formatting :bool=Field(description="If True, bold the headers and style them with white font on a black background")

class createExcelTool(BaseTool):
    name = "create_excel"
    description = """
        Useful when you want to create a Excel file.
        
        """
    args_schema: Type[BaseModel] = ExcelInputs
    def _run(self,data:dict, filename:str):
        response= create_excel(data, filename)
        return response +'The created Excel file was saved to the following Google Drive folder: https://drive.google.com/drive/folders/1hFtP-CaJTO0ILNPZA5rPmZqjiPXHQtHQ'
