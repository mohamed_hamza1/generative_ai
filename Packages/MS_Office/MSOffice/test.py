from docx import Document
from docx.shared import Pt
from docx.enum.text import WD_ALIGN_PARAGRAPH
from langchain.tools import BaseTool 
from pydantic import BaseModel, Field
def create_word_document(title, sections):
    """
    Creates a Word document with a title in the center and sections with bullets points.
    
    :param title: str, title of the document.
    :param sections: list of dict, each item should have a 'title' and 'paragraph' key.
    """
    doc = Document()
    
    # Add the main title
    title_paragraph = doc.add_heading(level=1)
    run = title_paragraph.add_run(title)
    run.font.size = Pt(24)  # Set the font size
    title_paragraph.alignment = WD_ALIGN_PARAGRAPH.CENTER  # Center alignment
    
    # Add sections
    for section in sections:
        # Error check to ensure both 'title' and 'paragraph' keys exist
        if 'title' in section and 'paragraph' in section:
            # Add section title
            sec_title = doc.add_heading(section['title'], level=2)
            run = sec_title.runs[0]
            run.font.size = Pt(20)  # Set the font size for section titles
            
            # Add paragraph
            doc.add_paragraph(section['paragraph'])
            
            # Insert page break after each section
            doc.add_page_break()    
    # Save the document
    doc.save(f"{title}.docx")
# class WordInputs(BaseModel):
#     """Inputs for create_word_document"""

#     title :str =Field(description="title of the document.")
#     sections :list=Field(description="each item should have a 'title' and 'paragraph'")

# class createWordTool(BaseTool):
#     name = "create_word_document"
#     description = """
#         Creates a Word document with a title in the center and sections with bullets points.
#         """
#     args_schema: Type[BaseModel] = WordInputs
#     def _run(self,title:str, sections:list):
#         create_word_document(title, sections)

sections_data = [
    {
        'title': 'Section 1',
        'paragraph': 'This is the content for section 1. Some more details can be added here.'
    },
    {
        'title': 'Section 2',
        'paragraph': 'This is the content for section 2. You can continue to describe the section here.'
    }
]

create_word_document('My Document Title', sections_data)
