from pptx import Presentation
from pptx.util import Inches, Pt
from pptx.dml.color import RGBColor
from langchain.tools import BaseTool 
from pydantic import BaseModel, Field
from typing import Optional, Type
from pptx import Presentation
from pptx.dml.color import RGBColor
from pptx.util import Pt
from googleapiclient.discovery import build
from googleapiclient.http import MediaFileUpload
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
import pickle
import os

def create_presentation(title, slides_content, save_path="asdddddd.pptx"):
    """
    Create a PowerPoint presentation.

    Args:
    - title (str): Title of the presentation.
    - slides_content (list of dicts): List containing dictionaries. Each dictionary represents a slide. 
                                    Dictionary keys:
                                    - "title" (str): Title of the slide.
                                    - "content" (str): Content of the slide.
    - save_path (str): Path to save the presentation.
    """
    
    # Create a new presentation object
    prs = Presentation()

    # Background color
    bg_color = RGBColor(64, 64, 64)  # 25% lighter than black

    # Blue Accent 1 color
    blue_accent_1 = RGBColor(93, 123, 157)

    # Create a title slide first
    slide_layout = prs.slide_layouts[0]
    slide = prs.slides.add_slide(slide_layout)
    
    # Set background color
    slide.background.fill.solid()
    slide.background.fill.fore_color.rgb = bg_color

    title_placeholder = slide.shapes.title
    subtitle_placeholder = slide.placeholders[1]

    title_placeholder.text = title
    title_placeholder.text_frame.paragraphs[0].font.color.rgb = blue_accent_1
    title_placeholder.text_frame.paragraphs[0].font.name = "Century Gothic"
    title_placeholder.text_frame.paragraphs[0].font.size = Pt(40)
    subtitle_placeholder.text_frame.paragraphs[0].font.color.rgb = RGBColor(255, 255, 255)  # White
    subtitle_placeholder.text_frame.paragraphs[0].font.size = Pt(28)

    # Add content slides
    for slide_content in slides_content:
        slide_layout = prs.slide_layouts[1]  # Using the 'Title and Content' slide layout
        slide = prs.slides.add_slide(slide_layout)
        
        # Set background color
        slide.background.fill.solid()
        slide.background.fill.fore_color.rgb = bg_color

        title_placeholder = slide.shapes.title
        body_placeholder = slide.placeholders[1]

        title_placeholder.text = slide_content["title"]
        title_placeholder.text_frame.paragraphs[0].font.color.rgb = blue_accent_1
        title_placeholder.text_frame.paragraphs[0].font.name = "Century Gothic"
        title_placeholder.text_frame.paragraphs[0].font.size = Pt(40)
        
        tf = body_placeholder.text_frame

        # Check if the content is a list, if so join into a single string with newlines.
        if isinstance(slide_content["content"], list):
            slide_content_text = '\n'.join(slide_content["content"])
        else:
            slide_content_text = slide_content["content"]

        tf.text = slide_content_text  # Set the joined text as the content for the slide

        # Set font size and color for the paragraphs
        for paragraph in tf.paragraphs:
            if paragraph == tf.paragraphs[0]:  # Assuming the first paragraph is the header
                paragraph.font.color.rgb = blue_accent_1
            else:
                paragraph.font.color.rgb = RGBColor(255, 255, 255)  # White
            paragraph.font.size = Pt(28)


    # Save the presentation
        prs.save(save_path)
        upload_file_to_google_drive(save_path,'https://drive.google.com/drive/folders/13to1sM0If3ctIrfXI4ZFU-dO67btMQ3-')
        return "Presentation created and saved successfully."

    
def get_folder_id_from_url(folder_url):
    """Extract the folder ID from a Google Drive folder URL."""
    # Assuming the URL is of the form: https://drive.google.com/drive/folders/{folder_id}
    return folder_url.split('/')[-1]

def upload_file_to_google_drive(filename, folder_url):
    """Uploads a file to a specified Google Drive folder by folder URL."""
    # If modifying these scopes, delete the file token.pickle.
    SCOPES = ['https://www.googleapis.com/auth/drive']

    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first time.
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file('Packages\Data_Scrapper\Cred5.json', SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    service = build('drive', 'v3', credentials=creds)

    folder_id = get_folder_id_from_url(folder_url)
    if folder_id is None:
        print(f"Folder URL '{folder_url}' is invalid.")
        return

    file_metadata = {'name': os.path.basename(filename), 'parents': [folder_id]}
    media = MediaFileUpload(filename, resumable=True)
    file = service.files().create(body=file_metadata, media_body=media, fields='id').execute()
    print(f"File ID: {file.get('id')}")


class PowerPointInputs(BaseModel):
    """Inputs for create_presentation"""

    title :str =Field(description="Title of the presentation.")
    slides_content: list[dict] =Field("List containing dictionaries. Each dictionary represents a slide ")
    save_path :str=Field("Path to save the presentation.")

class createPowePointTool(BaseTool):
    name = "create_presentation"
    description = """
        Design a PowerPoint presentation with title with a subtitle .
        The presentation should contain slides addressing 
        Each slide should have a clear title and present information in bullet points. 
        Each Slid need to contains at least 4 bullet points .
        ".
        """
    args_schema: Type[BaseModel] = PowerPointInputs
    def _run(self,title:str, slides_content:list[dict],save_path:str):
        response=create_presentation(title,slides_content,save_path)
        return response +'The created presentation file was saved to the following Google Drive folder: https://drive.google.com/drive/folders/13to1sM0If3ctIrfXI4ZFU-dO67btMQ3-'
