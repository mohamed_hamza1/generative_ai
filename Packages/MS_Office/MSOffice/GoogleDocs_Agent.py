from typing import Type
from pydantic import BaseModel, Field
from langchain.tools import BaseTool
from langchain.agents import AgentType,Tool,initialize_agent
from langchain.chat_models import ChatOpenAI
from Packages.MS_Office.MSOffice.Google_Docs import Create_Docs_Tool
from dotenv import load_dotenv
load_dotenv()

def GoogleDocs_Agent(objective):
    """Method to call Google Docs agent toolkit"""
    tools = [Create_Docs_Tool()]
    prompt = """
        You are an assistant that helps create a Google Docs file . You should always:
        - Be polite and helpful
        - Create Google Docs tool using the provided tools when asked
        - Make sure the data contain title and paragraphs 
        - Provide explanations for your actions
        - Add content as much as you can .
        - If no user description is provided, automatically include a relevant description using your knowledge base.
        - When ready to generate the Word document, provide a detailed description of the content you will include, organized into sections and subsections, if applicable. Ensure the document you describe is well-structured and meets the user's specified objective.
        User: {input}
        Assistant:
        """
    from Packages.Agents.Init_Agent import initialize_agent_tool
    agent = initialize_agent_tool()
    response = agent.init_agent(tools,prompt,objective,AgentType.OPENAI_FUNCTIONS)
    return (response)
 
class GoogleDocsAgentToolInput(BaseModel):
    """Inputs for GoogleDocs_Agent"""

    objective: str = Field(description="the objective required for agent prompt input")

  
class GoogleDocsAgentTool(BaseTool):
    name = "GoogleDocs_Agent"
    description = """
        The Google Docs Toolkit is designed for creating a Google Docs file.
        """
    args_schema: Type[BaseModel] = GoogleDocsAgentToolInput
    def get_description(self):
        return self.description
    
    def _run(self, objective: str) -> str:
        response = GoogleDocs_Agent(objective)
        return response

    def _arun(self, objective: str):
        raise NotImplementedError("GoogleDocs_Agent does not support async")