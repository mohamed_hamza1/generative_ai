from google_auth_oauthlib.flow import InstalledAppFlow
from google.oauth2.credentials import Credentials
from googleapiclient.discovery import build
from typing import Optional, Type
from langchain.tools import BaseTool 
from pydantic import BaseModel, Field
import os

import requests

def authenticate():
    scopes = ["https://www.googleapis.com/auth/documents", "https://www.googleapis.com/auth/drive.file"]

    # Check if token file exists and attempt to load it
    if os.path.exists('token.json'):
        try:
            creds = Credentials.from_authorized_user_file('token.json')
            if not creds.valid:
                if creds.expired and creds.refresh_token:
                    creds.refresh(requests())
                else:
                    raise ValueError("Credentials are not valid and cannot be refreshed.")
            return creds
        except Exception as e:
            print(f"Error loading token: {e}. Re-authenticating...")

    # If the above didn't return, we need to authenticate
    flow = InstalledAppFlow.from_client_secrets_file('Packages\MS_Office\MSOffice\Cred.json', scopes)
    creds = flow.run_local_server(port=0)
    with open('token.json', 'w') as token:
        token.write(creds.to_json())
    return creds


def create_google_doc(doc_title, content):
    """
    Creates a new Google Document, sets the title, and adds content.
    """
    authenticate()
    
    # Load the saved token
    creds = Credentials.from_authorized_user_file('token.json')
    service = build('docs', 'v1', credentials=creds)
    
    # Set the title of the document
    title = doc_title
    body = {'title': title}
    
    # Create a new document
    doc = service.documents().create(body=body).execute()
    
    # Print the title of the created document
    print('Created document with title:', doc.get('title'))
    
    # Add content to the document
    requests = [
        {
            'insertText': {
                'location': {
                    'index': 1,
                },
                'text': content
            }
        },
        {
            'updateParagraphStyle': {
                'range': {
                    'startIndex': 1,
                    'endIndex': len(title) + 1  # +1 for the newline character
                },
                'paragraphStyle': {
                    'alignment': 'CENTER'
                },
                'fields': 'alignment'
            }
        }
    ]
    service.documents().batchUpdate(documentId=doc['documentId'], body={'requests': requests}).execute()
    
    # print("Content added to the document.")
    
    return "Content added to the document."


class Google_docs_Inputs(BaseModel):
    """Inputs for create_google_doc"""

    title :str =Field(description="title of the google docs document.")
    content :str=Field(description="text for the content of the google docs document '")

class Create_Docs_Tool(BaseTool):
    name = "create_google_doc"
    description = """
    This utility facilitates the creation of a Google Docs document, emphasizing design and layout by positioning the 
    title at the center for a more professional and aesthetically pleasing look. Beyond just formatting,
     it's imperative to populate the document with well-researched and knowledgeable content. Leveraging this tool, you can establish a structured foundation for your document,
    allowing you to focus on delivering quality information that caters to your target audience's needs and interests.
    """

    args_schema: Type[BaseModel] = Google_docs_Inputs
    def _run(self,title:str, content:str):
        response=create_google_doc(title, content)
        return response


