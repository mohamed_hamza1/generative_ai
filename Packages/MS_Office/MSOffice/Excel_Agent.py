from typing import Type
from pydantic import BaseModel, Field
from langchain.tools import BaseTool
from langchain.agents import AgentType,Tool,initialize_agent
from langchain.chat_models import ChatOpenAI
from Packages.MS_Office.MSOffice.Excel import createExcelTool


def Excel_Agent(objective):
    """Method to call Excel agent """
    tools = [createExcelTool()]
    prompt = """
        You are an assistant that helps to g Create Excel File. You should always:
        - Take the user input, and list all the provided arguments within the user input.
        - Pass the arguments to the provided tools when being used
        - Be polite and helpful
        - Create Excel file through Microsoft createExcelTool using the provided tools when asked .
        - Provide explanations for your actions
        User: {input}
        Assistant:
        """
    from Packages.Agents.Init_Agent import initialize_agent_tool
    agent = initialize_agent_tool()
    response = agent.init_agent(tools,prompt,objective,AgentType.STRUCTURED_CHAT_ZERO_SHOT_REACT_DESCRIPTION)

    return (response)
 
class Excel_AgentToolInput(BaseModel):
    """Inputs for Excel_Agent"""

    objective: str = Field(description="the objective required for agent prompt input")

  
class ExcelAgentTool(BaseTool):
    name = "Excel_Agent"
    description = """
        Useful when you want Create Excel file .
        This tool is useful for creating Excel file do not use any other tools
        """
    args_schema: Type[BaseModel] = Excel_AgentToolInput
    def get_description(self):
        return self.description
    def _run(self, objective: str) -> str:
        response = Excel_Agent(objective)
        return response

    def _arun(self, objective: str):
        raise NotImplementedError("Excel_Agent does not support async")