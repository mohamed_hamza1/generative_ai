from typing import Type
from pydantic import BaseModel, Field
from langchain.tools import BaseTool
from langchain.agents import AgentType,Tool,initialize_agent
from langchain.chat_models import ChatOpenAI
from Packages.MS_Office.MSOffice.Word import createWordTool
from dotenv import load_dotenv
load_dotenv()

def Word_Agent(objective):
    """Method to call Word agent toolkit"""
    tools = [createWordTool()]
    prompt = """
        You are an assistant that helps create a word file . You should always:
        - Be polite and helpful
        - Create Word tool using the provided tools when asked
        - Make sure the data contain title and paragraphs 
        - Pass the title to the  word tool 
        - Provide explanations for your actions
        - Add content as much as you can .
        - If no user description is provided, automatically include a relevant description using your knowledge base.
        - When ready to generate the Word document, provide a detailed description of the content you will include, organized into sections and subsections, if applicable. Ensure the document you describe is well-structured and meets the user's specified objective.
        User: {input}
        Assistant:
        """
    from Packages.Agents.Init_Agent import initialize_agent_tool
    agent = initialize_agent_tool()
    response = agent.init_agent(tools,prompt,objective,AgentType.OPENAI_FUNCTIONS)
    return (response)
 
class WordAgentToolInput(BaseModel):
    """Inputs for Word_Agent"""

    objective: str = Field(description="the objective required for agent prompt input")

  
class WordAgentTool(BaseTool):
    name = "Word_Agent"
    description = """
        The Word Toolkit is designed for creating a word file.
        """
    args_schema: Type[BaseModel] = WordAgentToolInput
    def get_description(self):
        return self.description
    
    def _run(self, objective: str) -> str:
        response = Word_Agent(objective)
        return response

    def _arun(self, objective: str):
        raise NotImplementedError("Word_Agent does not support async")