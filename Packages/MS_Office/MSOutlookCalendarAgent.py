from typing import Type
from pydantic import BaseModel, Field
from langchain.tools import BaseTool
from langchain.agents import AgentType,Tool,initialize_agent
from langchain.chat_models import ChatOpenAI
import os
from Packages.MS_Office.OutlookCalendar import (
    create_event, cancel_event, get_event_list, search_by_event_subject, update_event, accept_event
)


from dotenv import load_dotenv
load_dotenv()

def MicrosoftCalendarOutlook_Agent(objective):
    """Method to call Microsoft calendar Outlook agent toolkit"""
    tools = [
        get_event_list.GetEventList(),
        create_event.CreateEvent(),
        cancel_event.CancelEvent(),
        search_by_event_subject.SearchByEventSubject(),
        update_event.UpdateEvent(),
        accept_event.AcceptEvent()
        ]
    prompt = """
        You are an assistant that helps to get access to micrsoft outlook Calendar tools. You should always:
        - Take the user input, and list all the provided arguments within the user input.
        - Pass the arguments to the provided tools when being used
        - dont generate any additional inputs
        - dont create onlineMeetingProvider when creating events
        - when you cancel an event please search first search about the event by its subject using SearchByEventSubject tool then get that event according to the result and take the event id and pass that id to CancelEvent tool 
        - while performing UpdateEvent please search first search about the event (SearchByEventSubject) then get that event id and pass that id to UpdateEvent tool 
        - in accepting an event you must get the id upon searching on the event by its subject using (SearchByEventSubject)
        - Be polite and helpful
        - GetEventList, CreateEvent, CancelEvent, SearchByEventSubject, UpdateEvent using the provided tools when asked
        - Provide explanations for your actions
        User: {input}
        Assistant:
        """
    from Packages.Agents.Init_Agent import initialize_agent_tool
    agent = initialize_agent_tool()
    response = agent.init_agent(tools,prompt,objective,AgentType.STRUCTURED_CHAT_ZERO_SHOT_REACT_DESCRIPTION)

    return (response)
    
class MicrosoftCalendarOutlookAgentToolInput(BaseModel):
    """Inputs for MicrosoftCalendarOutlook_Agent"""

    objective: str = Field(description="the objective required for agent prompt input")

  
class MicrosoftCalendarOutlookAgentTool(BaseTool):
    name = "MicrosoftCalendarOutlook_Agent"
    description = """
        Useful when you want to initialize the agent for Microsoft calendar Outlook Apps,  
        """
    args_schema: Type[BaseModel] = MicrosoftCalendarOutlookAgentToolInput
    def get_description(self):
        return self.description
    def _run(self, objective: str) -> str:
        response = MicrosoftCalendarOutlook_Agent(objective)
        return response

    def _arun(self, objective: str):
        raise NotImplementedError("MicrosoftOutlook_Agent does not support async")

