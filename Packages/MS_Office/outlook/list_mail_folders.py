from langchain.tools import BaseTool
import requests
import os
from dotenv import load_dotenv

# Load environment variables from .env file
load_dotenv()

def list_mail_folders():
    """Method to list all folders exist in your outlook mailbox"""

    token = os.environ.get("OUTLOOK_TOKEN")

    graph_data = requests.get(  # Use token to call downstream service
        "https://graph.microsoft.com/v1.0/me/mailFolders",
        headers={'Authorization': 'Bearer ' + token},
        ).json()

    # try:
    #   if graph_data["error"]:
    #     raise graph_data["error"]["message"]
    # except:
    return graph_data
    

class ListMailFolders(BaseTool):
    name = "list_mail_folders"
    description = """
        Useful when you want to list/get all mail folders is exist in your outlook mailbox
        """

    def _run(self, query: str=None) -> str:
        response = list_mail_folders()
        return response

