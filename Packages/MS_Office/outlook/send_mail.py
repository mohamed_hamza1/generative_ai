import os
import requests
import json
import mimetypes
import base64
from typing import Type
from pydantic import BaseModel, Field
from langchain.tools import BaseTool
from dotenv import load_dotenv

# Load environment variables from .env file
load_dotenv()



def send_mail_outlook(subject, body, to_recipient, cc_recipient=None, attachment_name=None, attachment_path=None):
    """Method to send mail using outlook account"""

    token = os.environ.get("OUTLOOK_TOKEN")

    data = {
        "message": {
            "subject": subject,
            "body": {
            "contentType": "Text",

            "content": body
            },
            "toRecipients":[]
        }
        }
    for email in to_recipient:
         data['message']['toRecipients'].append({
             "emailAddress": {
                "address": email
                }} )
         
    cc_recipient_len = len(cc_recipient) 
    if cc_recipient_len != 0:
        data["message"]["ccRecipients"] = [
            {
                 "emailAddress": {
                    "address": cc_recipient[0]
                }
           }
        ]
        if cc_recipient_len >1:
            for email_index in range(1, cc_recipient_len):
                data['message']['ccRecipients'].append({
                    "emailAddress": {
                        "address": cc_recipient[email_index]
                        }} )
                
    attachment_name_len = len(attachment_name)
    attachment_path_len = len(attachment_path)
    if attachment_name_len != 0 and attachment_path_len != 0:

        # Read the content of the file
        with open(attachment_path[0], "rb") as file:
            file_content = file.read()

        # Encode the content as base64
        content_bytes = base64.b64encode(file_content).decode('utf-8')

        # Get file MIME Type
        mime_type, _ = mimetypes.guess_type(attachment_name[0])
    
        data["message"]["attachments"] = [{
                "@odata.type": "#microsoft.graph.fileAttachment",
                "name": attachment_name[0],
                "contentType": mime_type,
                "contentBytes": content_bytes
            }]
        
        if attachment_name_len>1 and attachment_path_len>1:
            for name_index  in range(1, attachment_name_len):
                with open(attachment_path[name_index], "rb") as file:
                    file_content = file.read()

                # Encode the content as base64
                content_bytes = base64.b64encode(file_content).decode('utf-8')

                # Get file MIME Type
                mime_type, _ = mimetypes.guess_type(attachment_name[name_index])
            
                data['message']['attachments'].append({
                "@odata.type": "#microsoft.graph.fileAttachment",
                "name": attachment_name[name_index],
                "contentType": mime_type,
                "contentBytes": content_bytes
            })
            



    print(json.dumps(data))
    graph_data = requests.post(  # Use token to call downstream service
        "https://graph.microsoft.com/v1.0/me/sendMail",
        headers={
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token,
            },
        data=json.dumps(data),
        )

    # try:
    #   if graph_data["error"]:
    #     return "there is a problem accured please try again some other time"  
    # except:
    return graph_data

 
class SendOutlookMailInput(BaseModel):
    """Inputs for send_mail_outlook"""

    subject: str = Field(description="the subject of outlook mail")
    body: str = Field(description="the body of outlook mail")
    to_recipient: list = Field(description="the list of the recipient mails to send the mail to")
    cc_recipient: list = Field(description="the list of CC recipient mails to send the mail to, if it's not provided use the defaullt value", default=[])
    attachment_name: list = Field(description="the name of the attachemnt file that the user specify", default=[])
    attachment_path: list = Field(description="the path of the file and it should be passed to the function as it is", default=[])



class SendOutlookMail(BaseTool):
    name = "send_outlook_mail"
    description = """
        Useful when you want to send a mail to someone using your outlook account
        """
    args_schema: Type[BaseModel] = SendOutlookMailInput
    def _run(self, subject: str, body: str, to_recipient: list, cc_recipient:list=[], attachment_name:list=[], attachment_path:list=[]) -> str:
        response = send_mail_outlook(subject, body, to_recipient, cc_recipient, attachment_name, attachment_path)
        return response

