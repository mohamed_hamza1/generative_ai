import os
import requests
import json
import mimetypes
import base64
from typing import Type
from pydantic import BaseModel, Field
from langchain.tools import BaseTool
import os 
from dotenv import load_dotenv

# Load environment variables from .env file
load_dotenv()


def create_draft_replyall(msg_id, body, attachment_name=None, attachment_path=None):
    """Method to create draft reply all message using outlook account"""

    token = os.environ.get("OUTLOOK_TOKEN")

    data ={
            "message": {
                "body": {
                    "contentType": "Text",
                    "content": body
                }
            }
        }
 
    attachment_name_len = len(attachment_name)
    attachment_path_len = len(attachment_path)
    if attachment_name_len != 0 and attachment_path_len != 0:

        # Read the content of the file
        with open(attachment_path[0], "rb") as file:
            file_content = file.read()

        # Encode the content as base64
        content_bytes = base64.b64encode(file_content).decode('utf-8')

        # Get file MIME Type
        mime_type, _ = mimetypes.guess_type(attachment_name[0])
    
        data["attachments"] = [{
                "@odata.type": "#microsoft.graph.fileAttachment",
                "name": attachment_name[0],
                "contentType": mime_type,
                "contentBytes": content_bytes
            }]
        
        if attachment_name_len>1 and attachment_path_len>1:
            for name_index  in range(1, attachment_name_len):
                with open(attachment_path[name_index], "rb") as file:
                    file_content = file.read()

                # Encode the content as base64
                content_bytes = base64.b64encode(file_content).decode('utf-8')

                # Get file MIME Type
                mime_type, _ = mimetypes.guess_type(attachment_name[name_index])
            
                data['attachments'].append({
                "@odata.type": "#microsoft.graph.fileAttachment",
                "name": attachment_name[name_index],
                "contentType": mime_type,
                "contentBytes": content_bytes
            })
            



    print(json.dumps(data))
    graph_data = requests.post(  # Use token to call downstream service
        "https://graph.microsoft.com/v1.0/me/messages/"+msg_id+"/createReplyAll",
        headers={
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token,
            },
        data=json.dumps(data),
        )

    # try:
    #   if graph_data["error"]:
    #     return "there is a problem accured please try again some other time"  
    # except:
    return graph_data

 
class CreatDraftReplyAllInput(BaseModel):
    """Inputs for create_draft_replyall"""
    msg_id: str = Field(description="the id of the message you want to make a draft reply all ")
    body: str = Field(description="the body of outlook draft reply all mail ")
    attachment_name: list = Field(description="the name of the attachemnt file that the user specify", default=[])
    attachment_path: list = Field(description="the path of the file and it should be passed to the function as it is", default=[])



class CreatDraftReplyAll(BaseTool):
    name = "creat_draft_replyall"
    description = """
        Useful when you want to create a draft reply all message to a certain message using the mail id that is found upon searching on that message using your outlook account
        """
    args_schema: Type[BaseModel] = CreatDraftReplyAllInput
    def _run(self, msg_id: str, body: str, attachment_name:list=[], attachment_path:list=[]) -> str:
        response = create_draft_replyall(msg_id, body, attachment_name, attachment_path)
        return response

