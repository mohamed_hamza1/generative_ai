from langchain.tools import BaseTool
import requests
from typing import Type
from pydantic import BaseModel, Field
from langchain.tools import BaseTool
import os 
from dotenv import load_dotenv

# Load environment variables from .env file
load_dotenv()

def search_by_to_email_id(to_email):
    """Method to search inside an outlook mailbox for mails that you've sent to someone"""

    token = os.environ.get("OUTLOOK_TOKEN")


    graph_data = requests.get(  # Use token to call downstream service
        'https://graph.microsoft.com/v1.0/me/messages?$search="to:'+ to_email +'"&$select=sender,subject,bccRecipients,body,bodyPreview,ccRecipients,from,importance,isRead,sentDateTime,toRecipients',
        headers={'Authorization': 'Bearer ' + token},
        ).json()

    # try:
    #     if graph_data["error"]:
    #         return "there is a problem accured please try again some other time"  
    # except:
    return graph_data
    
class SearchByToEmailIDInput(BaseModel):
    """Inputs for search_by_to_email_id"""

    to_email: str = Field(description="the email id of the person that you've sent a message to 'the recipient mail'")

class SearchByToEmailID(BaseTool):
    name = "search_by_to_email_id"
    description = """
        Useful when you want to search inside your outlook mailbox and get the emails that you've sent to someone 'recipient' 
        """
    args_schema: Type[BaseModel] = SearchByToEmailIDInput
    def _run(self, to_email: str) -> str:
        response = search_by_to_email_id(to_email)
        return response

