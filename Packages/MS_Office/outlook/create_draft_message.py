import os
import requests
import json
import mimetypes
import base64
from typing import Type
from pydantic import BaseModel, Field
from langchain.tools import BaseTool
from dotenv import load_dotenv

# Load environment variables from .env file
load_dotenv()


def create_draft_message(subject, body, to_recipient, cc_recipient=None, attachment_name=None, attachment_path=None):
    """Method to create draft message using outlook account"""

    token = os.environ.get("OUTLOOK_TOKEN")

    data = {
            "subject": subject,
            "body": {
            "contentType": "Text",
            "content": body
            },
            "toRecipients":[]
        }
    
    if to_recipient[0]["emailAddress"]:
        data['toRecipients'].append({
             "emailAddress": to_recipient[0]["emailAddress"]} )
    else:
        for email in to_recipient:
            data['toRecipients'].append({
                "emailAddress": {
                    "address": email
                    }} )
            
    cc_recipient_len = len(cc_recipient) 
    if cc_recipient_len != 0:
        data["ccRecipients"] = [
            {
                 "emailAddress": {
                    "address": cc_recipient[0]
                }
           }
        ]
        if cc_recipient_len >1:
            for email_index in range(1, cc_recipient_len):
                data['ccRecipients'].append({
                    "emailAddress": {
                        "address": cc_recipient[email_index]
                        }} )
                
    attachment_name_len = len(attachment_name)
    attachment_path_len = len(attachment_path)
    if attachment_name_len != 0 and attachment_path_len != 0:

        # Read the content of the file
        with open(attachment_path[0], "rb") as file:
            file_content = file.read()

        # Encode the content as base64
        content_bytes = base64.b64encode(file_content).decode('utf-8')

        # Get file MIME Type
        mime_type, _ = mimetypes.guess_type(attachment_name[0])
    
        data["attachments"] = [{
                "@odata.type": "#microsoft.graph.fileAttachment",
                "name": attachment_name[0],
                "contentType": mime_type,
                "contentBytes": content_bytes
            }]
        
        if attachment_name_len>1 and attachment_path_len>1:
            for name_index  in range(1, attachment_name_len):
                with open(attachment_path[name_index], "rb") as file:
                    file_content = file.read()

                # Encode the content as base64
                content_bytes = base64.b64encode(file_content).decode('utf-8')

                # Get file MIME Type
                mime_type, _ = mimetypes.guess_type(attachment_name[name_index])
            
                data['attachments'].append({
                "@odata.type": "#microsoft.graph.fileAttachment",
                "name": attachment_name[name_index],
                "contentType": mime_type,
                "contentBytes": content_bytes
            })
            



    print(json.dumps(data))
    graph_data = requests.post(  # Use token to call downstream service
        "https://graph.microsoft.com/v1.0/me/messages",
        headers={
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token,
            },
        data=json.dumps(data),
        )

    # try:
    #   if graph_data["error"]:
    #     return "there is a problem accured please try again some other time"  
    # except:
    return graph_data

 
class CreatDraftMessageInput(BaseModel):
    """Inputs for create_draft_message"""

    subject: str = Field(description="the subject of outlook mail")
    body: str = Field(description="the body of outlook mail")
    to_recipient: list = Field(description="the list of the recipient email ids to send the mail to", examples=['example.test@gmail.com', 'test.example@example.com'])
    cc_recipient: list = Field(description="the list of CC recipient email ids to send the mail to, if it's not provided use the defaullt value", default=[])
    attachment_name: list = Field(description="the name of the attachemnt file that the user specify", default=[])
    attachment_path: list = Field(description="the path of the file and it should be passed to the function as it is", default=[])



class CreateDraftMessage(BaseTool):
    name = "creat_draft_message_outlook"
    description = """
        Useful when you want to create a draft message to someone that the user provided using your outlook account
        """
    args_schema: Type[BaseModel] = CreatDraftMessageInput
    def _run(self, subject: str, body: str, to_recipient: list, cc_recipient:list=[], attachment_name:list=[], attachment_path:list=[]) -> str:
        response = create_draft_message(subject, body, to_recipient, cc_recipient, attachment_name, attachment_path)
        return response

