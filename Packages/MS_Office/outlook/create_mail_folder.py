import requests
import json
from typing import Type
from pydantic import BaseModel, Field
from langchain.tools import BaseTool
import os 
from dotenv import load_dotenv

# Load environment variables from .env file
load_dotenv()


def create_mail_folder(folder_name):
    """Method to create a folder in outlook mail"""

    token = os.environ.get("OUTLOOK_TOKEN")

    data = {
        "displayName": folder_name
        }
            
    graph_data = requests.post(  # Use token to call downstream service
        "https://graph.microsoft.com/v1.0/me/mailFolders",
        headers={
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token,
            },
        data=json.dumps(data),
        )

    # try:
    #   if graph_data["error"]:
    #     return "there is a problem accured in create_mail_folder please try again some other time"  
    # except:
    return graph_data

 
class CreateMailFolderInput(BaseModel):
    """Inputs for create_mail_folder"""
    folder_name: str = Field(description="the name of the new mail folder")
  

class CreatemailFolder(BaseTool):
    name = "create_mail_folder"
    description = """
        Useful when you want to create a mail folder in your outlook mail 
        """
    args_schema: Type[BaseModel] = CreateMailFolderInput
    def _run(self, folder_name: str) -> str:
        response = create_mail_folder(folder_name)
        return response


    def _arun(self, folder_name: str):
        raise NotImplementedError("create_mail_folder does not support async")

