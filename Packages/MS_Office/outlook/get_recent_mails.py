from langchain.tools import BaseTool
import requests
import os 
from dotenv import load_dotenv

# Load environment variables from .env file
load_dotenv()


def get_recent_mails_outlook():
    """Method to get recent 10 mails in outlook mailbox"""

    token = os.environ.get("OUTLOOK_TOKEN")
    
    graph_data = requests.get(  # Use token to call downstream service
        "https://graph.microsoft.com/v1.0/me/messages?$select=sender,subject,bccRecipients,bodyPreview,ccRecipients,from,importance,isRead,sentDateTime,toRecipients&top=1",
        headers={'Authorization': 'Bearer ' + token},
        ).json()

    # try:
    #   if graph_data["error"]:
    #     return "there is a problem accured please try again some other time"  
    # except:
    return graph_data
    

class RecentMailsForOutlook(BaseTool):
    name = "get_recent_mails_outlook"
    description = """
        Useful when you want to get your recent mails that exist in your outlook mailbox
        """

    
    def _run(self, query: str) -> str:
        response = get_recent_mails_outlook()
        return response

