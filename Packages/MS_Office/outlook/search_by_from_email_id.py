import os 
import requests
from langchain.tools import BaseTool
from pydantic import BaseModel, Field
from typing import Type
from dotenv import load_dotenv

# Load environment variables from .env file
load_dotenv()


def search_by_from_email_id(from_email):
    """Method to search inside an outlook mailbox to find the mails that came from someone"""

    token = os.environ.get("OUTLOOK_TOKEN")


    graph_data = requests.get(  # Use token to call downstream service
        'https://graph.microsoft.com/v1.0/me/messages?$search="from:'+ from_email +'"&$select=sender,subject,bccRecipients,bodyPreview,ccRecipients,from,importance,isRead,sentDateTime,toRecipients',
        headers={'Authorization': 'Bearer ' + token},
        ).json()

    # try:
    #     if graph_data["error"]:
    #         return "there is a problem accured please try again some other time"  
    # except:

    return graph_data
    
class SearchByFromEmailIDInput(BaseModel):
    """Inputs for search_by_from_email_id"""

    from_email: str = Field(description="the email id of the sender or the email id of the person that have sent you an email ",examples=['menne.emad@smarttechsys.com', 'emaple@example.com'])

class SearchByFromEmailID(BaseTool):
    name = "search_by_from_email_id"
    description = """
        Useful when you want to search inside your outlook mailbox and get the emails that came from a certain person or a certain person have sent it to you (sender)
        """
    args_schema: Type[BaseModel] = SearchByFromEmailIDInput
    def _run(self, from_email: str) -> str:
        response = search_by_from_email_id(from_email)
        return response

