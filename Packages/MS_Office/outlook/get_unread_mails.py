from langchain.tools import BaseTool
import requests
import os 
from dotenv import load_dotenv

# Load environment variables from .env file
load_dotenv()


def get_unread_mails():
    """Method to get recent unread outlook mails"""

    token = os.environ.get("OUTLOOK_TOKEN")

    graph_data = requests.get(  # Use token to call downstream service
        'https://graph.microsoft.com/v1.0/me/messages?$search="isRead:false"&$select=sender,subject,bccRecipients,body,bodyPreview,ccRecipients,from,importance,isRead,sentDateTime,toRecipients',
        headers={'Authorization': 'Bearer ' + token},
        ).json()

    # try:
    #   if graph_data["error"]:
    #     return "there is a problem accured please try again some other time"  
    # except:
    # if graph_data["value"]==[]:
    #     return "there is no unread mails exist in your outlook mailbox, hoooray.."   
    # else:
    return graph_data

class UnreadMailsOutlook(BaseTool):
    name = "get_unread_mails"
    description = """
        Useful when you want to get your recent unread mails in your outlook mailbox you'll get the messages that is marked as unread and you've not read them yet 
        """
    
    def _run(self, query: str) -> str:
        response = get_unread_mails()
        return response

