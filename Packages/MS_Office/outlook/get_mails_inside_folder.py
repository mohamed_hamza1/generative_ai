import os 
import requests
from typing import Type
from dotenv import load_dotenv
from pydantic import BaseModel, Field
from langchain.tools import BaseTool
from langchain.tools import BaseTool
from Packages.MS_Office.outlook.list_mail_folders import list_mail_folders

# Load environment variables from .env file
load_dotenv()

def get_mails_inside_folder(folder_name):
    """Method to get all mails exist in a certain folder inside your outlook mailbox"""
    
    token = os.environ.get("OUTLOOK_TOKEN")
    folders_list = list_mail_folders()
    try:
        if folders_list["error"]:
            return folders_list["error"]
    except:
        folder_id=''

        if folders_list["value"]!=[]:
            folders_list = folders_list["value"]

        for folder in folders_list:
            if folder_name == folder["displayName"]:
                folder_id = folder["id"]
                break

        if folder_id:
            graph_data = requests.get(  # Use token to call downstream service
                "https://graph.microsoft.com/v1.0/me/mailFolders/"+folder_id+"/messages?$select=sender,subject,bccRecipients,body,bodyPreview,ccRecipients,from,importance,isRead,sentDateTime,toRecipients",
                headers={'Authorization': 'Bearer ' + token},
                ).json()
            # try:
            #     if graph_data["error"]:

            #         return graph_data["error"]  
            # except:
            #     if graph_data["value"]==[]:
            #         return "there is no messages exist in "+ folder_name   
            #     else:
            return graph_data
        else:
            return "there is no folder "+ folder_name +" exist in your outlook mailbox"   
        
    
class GetMailsInsideFolderInput(BaseModel):
    """Inputs for get_mails_inside_folder"""

    folder_name: str = Field(description="the folder name that you want to get your outlook mails from")

class GetMailsInsideFolder(BaseTool):
    name = "get_mails_inside_folder"
    description = """
        Useful when you want to get all mails inside a ceertain folder
        """
    args_schema: Type[BaseModel] = GetMailsInsideFolderInput
    def _run(self, folder_name: str) -> str:
        response = get_mails_inside_folder(folder_name)
        return response

