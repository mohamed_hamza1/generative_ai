from langchain.tools import BaseTool
import requests
from typing import Type
from pydantic import BaseModel, Field
from langchain.tools import BaseTool
import os 
from dotenv import load_dotenv

# Load environment variables from .env file
load_dotenv()

def search_by_keyword(keyword):
    """Method to search inside an outlook mail about a certain keyword that may be a name or exist inside body, subject, etc.."""

    token = os.environ.get("OUTLOOK_TOKEN")

    graph_data = requests.get(  # Use token to call downstream service
        "https://graph.microsoft.com/v1.0/me/messages?$search="+keyword+"&$select=sender,subject,bccRecipients,body,bodyPreview,ccRecipients,from,importance,isRead,sentDateTime,toRecipients",
        headers={'Authorization': 'Bearer ' + token},
        ).json()
  
    # try:
    #     if graph_data["error"]:
    #         return "there is a problem accured please try again some other time"  
    # except:
    # if graph_data["value"]==[]:
    #     return "there is no " + keyword + " exist in your outlook mailbox"   
    # else:
    return graph_data
    
class SearchByKeywordsInput(BaseModel):
    """Inputs for search_by_keyword"""

    keyword: str = Field(description="the keyword to search by inside your outlook mailbox")

class SearchByKeywords(BaseTool):
    name = "search_by_keyword"
    description = """
        Useful when you want to search inside your outlook mailbox by a keyword that could be name or work exist inside the body or the subject or even in body preview etc...
        """
    args_schema: Type[BaseModel] = SearchByKeywordsInput
    def _run(self, keyword: str) -> str:
        response = search_by_keyword(keyword)
        return response

