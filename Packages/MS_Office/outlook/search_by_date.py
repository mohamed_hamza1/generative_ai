from langchain.tools import BaseTool
import requests
from typing import Type
from pydantic import BaseModel, Field
from langchain.tools import BaseTool
from datetime import datetime, timedelta
import os 
from dotenv import load_dotenv

# Load environment variables from .env file
load_dotenv()


def increment_day(date_str):
    format_str = "%Y-%m-%d"
    try:
        # Parse the input date string using the provided format
        date = datetime.strptime(date_str, format_str)
        
        # Increment the day by 1
        new_date = date + timedelta(days=1)
        
        # Format the result as a string using the same format
        new_date_str = new_date.strftime(format_str)
        
        return new_date_str
    except ValueError:
        return "Invalid input date or format"




def search_by_date(start_date, end_date):
    """Method to search inside an outlook mailbox for mails that you have sent or recieved in a certain date or day"""

    token = os.environ.get("OUTLOOK_TOKEN")

    if not end_date:
        end_date = increment_day(start_date)
    else:
        end_date = increment_day(end_date)
        
    graph_data = requests.get(  # Use token to call downstream service
        'https://graph.microsoft.com/v1.0/me/messages?$filter=sentDateTime ge '+ start_date +' and sentDateTime lt '+ end_date +' &$select=sender,subject,bccRecipients,body,bodyPreview,ccRecipients,from,importance,isRead,sentDateTime,toRecipients',
        headers={'Authorization': 'Bearer ' + token},
        ).json()
    # print(graph_data)
    # try:
    #     if graph_data["error"]:
    #         return "there is a problem accured please try again some other time"  
    # except:
    return graph_data
    
class SearchByDateInput(BaseModel):
    """Inputs for search_by_date"""

    start_date: str = Field(description="the date (%Y-%m-%d) you want to filter your mails by or the start date of your time period you want to search in your mails by.")
    end_date: str = Field(description="the end date (%Y-%m-%d) of your time period you want to search in your mails by.", default='')


class SearchByDate(BaseTool):
    name = "search_by_date"
    description = """
        Useful when you want to search inside your outlook mailbox by a certain date or a time period' 
        """
    args_schema: Type[BaseModel] = SearchByDateInput
    def _run(self, start_date: str, end_date: str=''):
        response = search_by_date(start_date, end_date)
        return response

