import os
import requests
import json
import mimetypes
import base64
from typing import Type
from pydantic import BaseModel, Field
from langchain.tools import BaseTool
import os 
from dotenv import load_dotenv

# Load environment variables from .env file
load_dotenv()


def create_draft_forward(msg_id, to_recipient):
    """Method to create draft forward message using outlook account"""

    token = os.environ.get("OUTLOOK_TOKEN")

    data = {
            "message": {
                 "toRecipients":[]
            }
        }
    if to_recipient[0]["address"]:
        to_recipient[0] = to_recipient[0]["address"]
        email_address = to_recipient
        data['message']['toRecipients'].append(
            {
                "emailAddress":{
                    "address": email_address[0]
        }
            })
    elif to_recipient[0]:
        print(to_recipient)
        type(to_recipient)
        email_address = to_recipient
        data['message']['toRecipients'].append(
            {
                "emailAddress":{
                    "address": email_address[0]
        }
            })
    else:
        for email in to_recipient:
            data['message']['toRecipients'].append({
                "emailAddress": {
                    "address": email
                    }} )
            

    print(json.dumps(data))
    graph_data = requests.post(  # Use token to call downstream service
        "https://graph.microsoft.com/v1.0/me/messages/"+msg_id+"/createForward",
        headers={
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token,
            },
        data=json.dumps(data),
        )

    # try:
    #   if graph_data["error"]:
    #     return "there is a problem accured please try again some other time"  
    # except:
    return graph_data

 
class CreatDraftForwardInput(BaseModel):
    """Inputs for create_draft_forward"""
    msg_id: str = Field(description="the id of the message you want to make a draft forwad to ")
    to_recipient: list = Field(description="the list of the recipient email ids to send the forwarded mail to", examples=['example.test@gmail.com', 'test.example@example.com'])
   



class CreatDraftForward(BaseTool):
    name = "creat_draft_forward"
    description = """
        Useful when you want to create a draft Forward message to a certain message using the mail id that is found upon searching on that message using your outlook account
        """
    args_schema: Type[BaseModel] = CreatDraftForwardInput
    def _run(self, msg_id: str, to_recipient:list) -> str:
        response = create_draft_forward(msg_id, to_recipient)
        return response

