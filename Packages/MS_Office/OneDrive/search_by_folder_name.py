import requests
import os 
from dotenv import load_dotenv
from typing import Type
from pydantic import BaseModel, Field
from langchain.tools import BaseTool
# Load environment variables from .env file
load_dotenv()

def search_by_folder_name(folder_name):
    
    token = os.environ.get("OUTLOOK_TOKEN")

    # https://graph.microsoft.com/v1.0/me/drive/root:/test?select=name,id,webUrl,parentReference
    upload_url = "https://graph.microsoft.com/v1.0/me/drive/root:/"+folder_name+"?select=name,id,webUrl,parentReference"

    headers = {
        'Authorization': 'Bearer ' + token
    }

    response = requests.get(upload_url, headers=headers).json()
    print(response)

    return response
# print(search_by_folder_name('test'))

class SearchByFolderNameInput(BaseModel):
    """Inputs for search_by_folder_name"""

    folder_name: str = Field(description="the folder name you find in your one drive")
  

class SearchByFolderName(BaseTool):
    name = "search_by_folder_name"
    description = """
        Useful when you want to find a folder in your one drive 
        """
    args_schema: Type[BaseModel] = SearchByFolderNameInput
    def _run(self, folder_name:str) -> str:
        response = search_by_folder_name(folder_name)
        return response

