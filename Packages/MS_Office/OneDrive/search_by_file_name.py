import requests
import os 
from dotenv import load_dotenv
from typing import Type
from pydantic import BaseModel, Field
from langchain.tools import BaseTool
# Load environment variables from .env file
load_dotenv()

def search_by_file_name(file_name):
    
    token = os.environ.get("OUTLOOK_TOKEN")

    upload_url = "https://graph.microsoft.com/v1.0/me/drive/root/search(q='"+file_name+"')?select=name,id,webUrl,parentReference"

    headers = {
        'Authorization': 'Bearer ' + token
    }

    response = requests.get(upload_url, headers=headers).json()
    print(response)

    return response
# print(search_by_file_name('test'))

class SearchByFileNameInput(BaseModel):
    """Inputs for search_by_file_name"""

    file_name: str = Field(description="the file name you find in your one drive")
  

class SearchByFileName(BaseTool):
    name = "search_by_file_name"
    description = """
        Useful when you want to find a file in your one drive 
        """
    args_schema: Type[BaseModel] = SearchByFileNameInput
    def _run(self, file_name:str) -> str:
        response = search_by_file_name(file_name)
        return response

