import requests
import os 
import json
import mimetypes
import base64
from dotenv import load_dotenv
from typing import Type
from pydantic import BaseModel, Field
from langchain.tools import BaseTool
# Load environment variables from .env file
load_dotenv()

def upload_file(attachment_name, attachment_path, file_name=None, folder='children'):
    token = os.environ.get("OUTLOOK_TOKEN")
    fileName = file_name if file_name else attachment_name

    upload_url = 'https://graph.microsoft.com/v1.0/me/drive/root:/'+folder+'/'+fileName+':/content'

    headers = {
        'Authorization': 'Bearer ' + token
    }

    with open( attachment_path, 'rb') as file:
        response = requests.put(upload_url, headers=headers, data=file)
        print(response)

    if response.status_code == 200 or response.status_code == 201:
        return 'File uploaded successfully!'
    else:
        return('File upload failed with status code:', response.status_code)

 
class UploadFileInput(BaseModel):
    """Inputs for upload_files"""
 
    attachment_name: str = Field(description="the attachment name of the file that the user want to upload in his one drive folder")
    attachment_path: str = Field(description="the attachment path of the file that the user want to upload in his one drive folder")
    file_name: str = Field(description="the new file name that will be added as in the one drive folder", default='')
    folder: str = Field(description="the folder name that the user want to add the file into", default='children')
  

class UploadFile(BaseTool):
    name = "upload_file"
    description = """
        Useful when you want to upload a file in your one drive root folder or even another existing folder in your one drive
        """
    args_schema: Type[BaseModel] = UploadFileInput
    def _run(self, attachment_name:str, attachment_path:str, file_name:str=None, folder:str='children' ) -> str:
        response = upload_file(attachment_name, attachment_path, file_name, folder)
        return response

