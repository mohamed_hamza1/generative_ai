from typing import Type
from pydantic import BaseModel, Field
from langchain.tools import BaseTool
from langchain.agents import AgentType
from Packages.MS_Office.OneDrive import search_by_file_name,search_by_folder_name,upload_file


def OneDrive_Agent(objective):
    """Method to call Onedrive agent """
    tools = [search_by_file_name.SearchByFileName(),search_by_folder_name.SearchByFolderName(),upload_file.UploadFile()]
    prompt = """
        You are an assistant that helps to get access to microft outlook tools. You should always:
        - Take the user input, and list all the provided arguments within the user input.
        - Pass the arguments to the provided tools when being used
        - Be polite and helpful
        - Search by file name through OneDrive SearchByFileName using the provided tools when asked
        - Search by folder name through OneDrive SearchByFolderName using the provided tools when asked
        - Upload file through OneDrive UploadFile using the provided tools when asked
        - Provide explanations for your actions
        User: {input}
        Assistant:
        """
    from Packages.Agents.Init_Agent import initialize_agent_tool
    agent = initialize_agent_tool()
    response = agent.init_agent(tools,prompt,objective,AgentType.STRUCTURED_CHAT_ZERO_SHOT_REACT_DESCRIPTION)

    return (response)
 
class OneDriveToolInput(BaseModel):
    """Inputs for OneDrive_Agent"""

    objective: str = Field(description="the objective required for agent prompt input")

  
class OneDriveAgentTool(BaseTool):
    name = "MSOffice_Agent"
    description = """
        Useful when you want to initialize the agent for OneDrive tool for seacrhing or uploading files
        """
    args_schema: Type[BaseModel] = OneDriveToolInput
    def get_description(self):
        return self.description
    def _run(self, objective: str) -> str:
        response = OneDrive_Agent(objective)
        return response