from typing import Type
from pydantic import BaseModel, Field
from langchain.tools import BaseTool
from langchain.agents import AgentType,Tool,initialize_agent
from langchain.chat_models import ChatOpenAI
import os
from Packages.MS_Office.outlook import (
create_draft_forward,create_draft_message,create_draft_reply,create_draft_replyall,create_mail_folder,get_mails_inside_folder,
get_recent_mails,get_unread_mails,list_mail_folders,search_by_date,search_by_from_email_id,search_by_keyword,search_by_to_email_id,send_mail
)
from dotenv import load_dotenv
load_dotenv()

def MicrosoftOutlook_Agent(objective):
    """Method to call MicrosoftOutlook agent toolkit"""
    tools = [
        create_draft_forward.CreatDraftForward(),
        create_draft_message.CreateDraftMessage(),
        create_draft_reply.CreatDraftReply(),
        create_draft_replyall.CreatDraftReplyAll(),
        create_mail_folder.CreatemailFolder(),
        get_mails_inside_folder.GetMailsInsideFolder(),
        get_recent_mails.RecentMailsForOutlook(),
        get_unread_mails.UnreadMailsOutlook(),
        list_mail_folders.ListMailFolders(),
        search_by_date.SearchByDate(),
        search_by_to_email_id.SearchByToEmailID(),
        search_by_from_email_id.SearchByFromEmailID(),
        search_by_keyword.SearchByKeywords(),
        send_mail.SendOutlookMail()
        ]
    prompt = """
        You are an assistant that helps to get access to microft outlook tools. You should always:
        - Take the user input, and list all the provided arguments within the user input.
        - Pass the arguments to the provided tools when being used
        - Be polite and helpful
        - Create Draft message, forward, reply, or replyall using the provided tools when asked
        - Create Mail Folder using the provided tools when asked
        - Get Recent Mails, Unread Mails, or get Mails inside Folder using the provided tools when asked
        - List Mail folders using the provided tools when asked
        - Search for Emails, By_date, by_from_email_id, by_Keyword, by_to_email_id using the provided tools when asked
        - Send Email message using the provided tools when asked
        - Provide explanations for your actions
        User: {input}
        Assistant:
        """
    from Packages.Agents.Init_Agent import initialize_agent_tool
    agent = initialize_agent_tool()
    response = agent.init_agent(tools,prompt,objective,AgentType.STRUCTURED_CHAT_ZERO_SHOT_REACT_DESCRIPTION)

    return (response)
    
class MicrosoftOutlookAgentToolInput(BaseModel):
    """Inputs for MicrosoftOutlook_Agent"""

    objective: str = Field(description="the objective required for agent prompt input")

  
class MicrosoftOutlookAgentTool(BaseTool):
    name = "MicrosoftOutlook_Agent"
    description = """
        Useful when you want to initialize the agent for MicrosoftOutlook Apps,  
        """
    args_schema: Type[BaseModel] = MicrosoftOutlookAgentToolInput
    def get_description(self):
        return self.description
    def _run(self, objective: str) -> str:
        response = MicrosoftOutlook_Agent(objective)
        return response

    def _arun(self, objective: str):
        raise NotImplementedError("MicrosoftOutlook_Agent does not support async")

