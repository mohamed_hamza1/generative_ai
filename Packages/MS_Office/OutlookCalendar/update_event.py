import os
import requests
import json
import mimetypes
import base64
from typing import Type
from pydantic import BaseModel, Field
from langchain.tools import BaseTool
from datetime import datetime, timedelta
from dotenv import load_dotenv

# Load environment variables from .env file
load_dotenv()


def update_event(event_id, subject=None, body=None, start_datetime=None, timezone=None, end_datetime=None, attendees=[], location=None, is_online=None, online_provider=None):
    """Method to update an event in outlook calendar"""

    token = os.environ.get("OUTLOOK_TOKEN")

    data = {}
    if subject:
        data["subject"] = subject
    if body:
        data["body"] ={
        "content": body
    }
    if attendees!=[] :
        data['attendees']=[]
        for email in attendees:
            data['attendees'].append({
                "emailAddress": {
                    "address": email
                    }} )
    if start_datetime and timezone:
        data["start"] = {
                "dateTime": start_datetime,
                "timeZone": timezone
            }
    if end_datetime and timezone:
        data["end"] = {
                "dateTime": end_datetime,
                "timeZone": timezone
            }
    if location!='' or location:
        data['location'] = {
                "displayName": location
            }
    if is_online==True:
        data['isOnlineMeeting'] = is_online
        data['onlineMeetingProvider'] = online_provider
 

    print(json.dumps(data))
    graph_data = requests.patch(  # Use token to call downstream service
        "https://graph.microsoft.com/v1.0/me/events/"+event_id,
        headers={
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token,
            },
        data=json.dumps(data),
        )

    return graph_data
# print(update_event('weekly meeting', 'hey its me your team leader', ['menna.e.fahmy@gmail.com'], '2023-11-04T12:00:00', 'GMT'))
 
class UpdateEventInput(BaseModel):
    """Inputs for update_event"""

    event_id: str = Field(description="the id of the event you want update in your outook calendar")
    subject: str = Field(description="the subject of outlook event", default='')
    body: str = Field(description="the body of outlook event", default='')
    start_datetime: str = Field(description="the start date time field to the outlook event", examples=["2017-09-04T12:00:00", "2017-04-15T12:00:00"], default='')
    timezone: str = Field(description="the user time zone if its not provided search about it and get it for the user", default='')
    end_datetime: str = Field(description="the end date time field to the outlook event", default='', examples=["2017-09-04T12:00:00", "2017-04-15T12:00:00"])
    attendees: list = Field(description="the list of the attendees email id to send the event to", examples=['example6@example.com', 'info12@example.com'], default=[])
    location: str = Field(description="the name location of the event", default='')
    is_online: bool = Field(description="the attributes that identify if the event is online or not its is a a boolean field", default=False, examples=[True, False])
    online_provider: str = Field(description="the name of the online meeting provider", default='', examples=['teamsForBusiness', 'skypeForBusiness', 'skypeForConsumer'])


class UpdateEvent(BaseTool):
    name = "update_event_outlook"
    description = """
        Useful when you want to update an event in your outlook calendar
        """
    args_schema: Type[BaseModel] = UpdateEventInput
    def _run(self,event_id:str, subject: str='', body: str='', start_datetime:str='', timezone:str='', end_datetime:str='', attendees: list=[], location:str='', is_online:bool=False, online_provider:str=None) -> str:
        response = update_event(event_id, subject, body, start_datetime, timezone, end_datetime, attendees, location, is_online, online_provider)
        return response

