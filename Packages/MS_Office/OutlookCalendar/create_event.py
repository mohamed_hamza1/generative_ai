import os
import requests
import json
import mimetypes
import base64
from typing import Type
from pydantic import BaseModel, Field
from langchain.tools import BaseTool
from datetime import datetime, timedelta
from dotenv import load_dotenv

# Load environment variables from .env file
load_dotenv()


def create_event(subject, body, start_datetime, timezone, end_datetime=None, attendees=[], location=None, is_online=None, online_provider=None):
    """Method to create an event in outlook calendar"""

    token = os.environ.get("OUTLOOK_TOKEN")


    if not end_datetime or end_datetime =='':
          start_datetime = datetime.strptime(start_datetime, '%Y-%m-%dT%H:%M:%S')
          end_datetime = start_datetime + timedelta(minutes=30)
          print(start_datetime)
          print(end_datetime)
    start_datetime = str(start_datetime)
    end_datetime = str(end_datetime)
    data = {
            "subject": subject,
            "body": {
                "contentType": "Text",
                "content": body
            },
            "start": {
                "dateTime": start_datetime,
                "timeZone": timezone
            },
            "end": {
                "dateTime": end_datetime,
                "timeZone": timezone
            }
          
            }
    if attendees!=[] :
        data['attendees']=[]
        for email in attendees:
            data['attendees'].append({
                "emailAddress": {
                    "address": email
                    }} )

    if location!='' or location:
        data['location'] = {
                "displayName": location
            }
    if is_online:
        data['isOnlineMeeting'] = is_online

    if online_provider:
        data['onlineMeetingProvider'] = online_provider

    print(json.dumps(data))
    graph_data = requests.post(  # Use token to call downstream service
        "https://graph.microsoft.com/v1.0/me/events",
        headers={
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token,
            },
        data=json.dumps(data),
        )

    return graph_data
# print(create_event('weekly meeting', 'hey its me your team leader', ['menna.e.fahmy@gmail.com'], '2023-11-04T12:00:00', 'GMT'))
 
class CreateEventInput(BaseModel):
    """Inputs for create_event"""

    subject: str = Field(description="the subject of outlook event")
    body: str = Field(description="the body of outlook event")
    start_datetime: str = Field(description="the start date time field to the outlook event", examples=["2017-09-04T12:00:00", "2017-04-15T12:00:00"])
    timezone: str = Field(description="the user time zone if its not provided search about it and get it for the user",)
    end_datetime: str = Field(description="the end date time field to the outlook event", default='', examples=["2017-09-04T12:00:00", "2017-04-15T12:00:00"])
    attendees: list = Field(description="the list of the attendees email id to send the event to", examples=['example6@example.com', 'info12@example.com'], default=[])
    location: str = Field(description="the name location of the event", default='')
    is_online: bool = Field(description="the attributes that identify if the event is online or not its is a a boolean field", default=False, examples=[True, False])
    online_provider: str = Field(description="the name of the online meeting provider", default='', examples=['Microsoft Teams', 'Google Meet', 'Zoom'])


class CreateEvent(BaseTool):
    name = "create_event_outlook"
    description = """
        Useful when you want to create an event in your outlook calendar
        """
    args_schema: Type[BaseModel] = CreateEventInput
    def _run(self, subject: str, body: str, start_datetime:str, timezone:str, end_datetime:str='', attendees: list=[], location:str='', is_online:bool=False, online_provider:str='') -> str:
        response = create_event(subject, body, start_datetime, timezone, end_datetime, attendees, location, is_online, online_provider)
        return response

