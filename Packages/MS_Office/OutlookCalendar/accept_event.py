import os
import requests
import json
import mimetypes
import base64
from typing import Type
from pydantic import BaseModel, Field
from langchain.tools import BaseTool
import os 
from dotenv import load_dotenv

# Load environment variables from .env file
load_dotenv()


def accept_event(event_id, accept_message=None):
    """Method to accept an event using within your outlook calendar"""

    token = os.environ.get("OUTLOOK_TOKEN")

    if accept_message:
        data = {
                "Comment": accept_message
                }
    else:
        data={}
    
    print(json.dumps(data))
    graph_data = requests.post(  # Use token to call downstream service
        "https://graph.microsoft.com/v1.0/me/events/"+event_id+"/accept",
        headers={
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token,
            },
        data=json.dumps(data),
        )

    return graph_data

 
class AcceptEventInput(BaseModel):
    """Inputs for accept_event"""

    event_id: str = Field(description="the id of the event that the user want to accept")
    accept_message: str = Field(description="the message or the comment that works as an accept reason", default='')
   



class AcceptEvent(BaseTool):
    name = "accept_event"
    description = """
        Useful when you want to accept an event in your outlook calender
        """
    args_schema: Type[BaseModel] = AcceptEventInput
    def _run(self, event_id: str, accept_message:str=None) -> str:
        response = accept_event(event_id, accept_message)
        return response

