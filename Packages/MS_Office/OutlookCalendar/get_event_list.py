from langchain.tools import BaseTool
import requests
import os 
from dotenv import load_dotenv

# Load environment variables from .env file
load_dotenv()


def get_event_list():
    """Method to get event in your outlook calendar"""

    token = os.environ.get("OUTLOOK_TOKEN")
    
    graph_data = requests.get(
        "https://graph.microsoft.com/v1.0/me/events?$select=id,subject,bodyPreview,organizer,attendees,start,end,location,isOnlineMeeting",
        headers={'Authorization': 'Bearer ' + token},
        ).json()

    return graph_data
    

class GetEventList(BaseTool):
    name = "get_event_list"
    description = """
        Useful when you want to get events in your outlook calendar
        """

    
    def _run(self, query: str) -> str:
        response = get_event_list()
        return response

