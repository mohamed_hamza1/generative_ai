import os
import requests
import json
import mimetypes
import base64
from typing import Type
from pydantic import BaseModel, Field
from langchain.tools import BaseTool
import os 
from dotenv import load_dotenv

# Load environment variables from .env file
load_dotenv()


def cancel_event(event_id, cancellation_message=None):
    """Method to cancel an event within your outlook calendar"""

    token = os.environ.get("OUTLOOK_TOKEN")

    if cancellation_message:
        data = {
                "Comment": cancellation_message
                }
    else:
        data={}
    
    print(json.dumps(data))
    graph_data = requests.post(  # Use token to call downstream service
        "https://graph.microsoft.com/v1.0/me/events/"+event_id+"/cancel",
        headers={
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token,
            },
        data=json.dumps(data),
        )

    return graph_data

 
class CancelEvantInput(BaseModel):
    """Inputs for cancel_event"""

    event_id: str = Field(description="the id of the event that the user want to cancel")
    cancellation_message: str = Field(description="the message or the comment that works as cancellation reason", default='')
   



class CancelEvent(BaseTool):
    name = "cancel_event"
    description = """
        Useful when you want to cancel an event in your outlook calender
        """
    args_schema: Type[BaseModel] = CancelEvantInput
    def _run(self, event_id: str, cancellation_message:str=None) -> str:
        response = cancel_event(event_id, cancellation_message)
        return response

