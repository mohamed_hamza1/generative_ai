from langchain.tools import BaseTool
import requests
from typing import Type
from pydantic import BaseModel, Field
from langchain.tools import BaseTool
import os 
from dotenv import load_dotenv

# Load environment variables from .env file
load_dotenv()

def search_by_event_subject(event_subject):
    """Method to search inside an outlook calendar about an event that has a certain subject"""

    token = os.environ.get("OUTLOOK_TOKEN")

    graph_data = requests.get(  # Use token to call downstream service
        "https://graph.microsoft.com/v1.0/me/events?$filter=subject eq '"+ event_subject+"'$select=id,subject,bodyPreview,organizer,attendees,start,end,location,isOnlineMeeting",
        headers={'Authorization': 'Bearer ' + token},
        ).json()
    print(graph_data)
    # if graph_data['value']==[]:
    #     return "there is no email with subject "+ event_subject
    # return graph_data['value'][0]['id'] 
    return graph_data
    
class SearchByEventSubjectInput(BaseModel):
    """Inputs for search_by_event_subject"""

    event_subject: str = Field(description="the event subject to search by inside your outlook calendar")

class SearchByEventSubject(BaseTool):
    name = "search_by_event_subject"
    description = """
        Useful when you want to search inside your outlook calendar by the event subject
        """
    args_schema: Type[BaseModel] = SearchByEventSubjectInput
    def _run(self, event_subject: str) -> str:
        response = search_by_event_subject(event_subject)
        return response

