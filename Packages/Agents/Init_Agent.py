from langchain.chat_models import ChatOpenAI
from langchain.agents import AgentType,Tool,initialize_agent
from langchain.callbacks import get_openai_callback
from dotenv import load_dotenv
load_dotenv()

class initialize_agent_tool():
    LLM_name = "gpt-3.5-turbo-1106"
    # model_name = "gpt-4"
    llm = ChatOpenAI(model_name=LLM_name, temperature=0)
    used_tokens = 0
    def init_agent(self,tools,prompt,objective,agent):
        agent = initialize_agent(
            tools=tools,
            llm=self.llm,
            prompt = prompt,
            agent=agent, 
            verbose=True,
            max_tokens = 16000
            )
        
        with get_openai_callback() as cb:
            response = agent.run(objective)
        # Access the token usage information
        total_tokens = cb.total_tokens
        initialize_agent_tool.used_tokens += total_tokens
        # Print the token usage
        print(f"Used Tokens: {total_tokens}")
        return response
    def Current_Tokens(self):
        used_tokens = self.used_tokens
        return used_tokens
        
    
