from typing import Type
from pydantic import BaseModel, Field
from langchain.tools import BaseTool
from langchain.agents import AgentType,Tool,initialize_agent
from langchain.chat_models import ChatOpenAI
from dotenv import load_dotenv
from Packages.Jira.tools import *
load_dotenv()

def Jira_Agent(objective):
    """Method to call Jira agent toolkit"""
    tools = [CreateProjectTool(),CreateIssueTool(),AuthenticateUserTool()]
    prompt = """
            You are an assistant that helps create projects and issues. You should always:
            - Authenticate the user before taking any other action. You can use AuthenticateUserTool.
            - Be polite and helpful
            - Create projects and issues using the provided tools when asked
            - Provide explanations for your actions
            - Provide explanations for your actions
            User: {input}
            Assistant:
            """
    from Packages.Agents.Init_Agent import initialize_agent_tool
    agent = initialize_agent_tool()
    response = agent.init_agent(tools,prompt,objective,AgentType.STRUCTURED_CHAT_ZERO_SHOT_REACT_DESCRIPTION)

    return (response)
 
class JiraAgentToolInput(BaseModel):
    """Inputs for Jira_Agent"""

    objective: str = Field(description="the objective required for agent prompt input")

  
class JiraAgentTool(BaseTool):
    name = "Jira_Agent"
    description = """
        The Jira Apps Toolkit is designed for efficient system integrations. By initializing the Jira agent, users gain access to a suite of utilities including the CreateProjectTool(), CreateIssueTool(), and AuthenticateUserTool(). These tools streamline processes, enhancing user experience and operational effectiveness within the Jira environment.
        """
    args_schema: Type[BaseModel] = JiraAgentToolInput
    def get_description(self):
        return self.description
    def _run(self, objective: str) -> str:
        response = Jira_Agent(objective)
        return response

    def _arun(self, objective: str):
        raise NotImplementedError("Jira_Agent does not support async")