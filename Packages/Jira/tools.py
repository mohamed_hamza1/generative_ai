import json
from typing import Type
from urllib.parse import urlencode
from pydantic import BaseModel, Field
from langchain.tools import BaseTool
from dotenv import load_dotenv
import requests
# https://auth.atlassian.com/authorize?audience=api.atlassian.com&client_id=FDs2XWwxqm3NUC1HCiJtnu78iQt4bApZ&scope=write%3Aproject%3Ajira%20delete%3Aproject%3Ajira%20read%3Aproject-category%3Ajira%20write%3Aproject-category%3Ajira%20read%3Aproject.email%3Ajira%20read%3Aproject%3Ajira%20read%3Aissue-type%3Ajira%20read%3Aproject.property%3Ajira%20read%3Auser%3Ajira%20read%3Aapplication-role%3Ajira%20read%3Aavatar%3Ajira%20read%3Agroup%3Ajira%20read%3Aissue-type-hierarchy%3Ajira%20read%3Aproject-version%3Ajira%20read%3Aproject.component%3Ajira%20write%3Aissue%3Ajira%20write%3Acomment%3Ajira%20write%3Acomment.property%3Ajira%20write%3Aattachment%3Ajira%20read%3Aissue%3Ajira%20write%3Aissue-type%3Ajira%20read%3Aproject-type%3Ajira&redirect_uri=http%3A%2F%2F127.0.0.1%3A8000%2Fjira-management%2Fjira-auth%2F&state=${YOUR_USER_BOUND_VALUE}&response_type=code&prompt=consent
load_dotenv()
# Authorization = "eyJraWQiOiJmZTM2ZThkMzZjMTA2N2RjYTgyNTg5MmEiLCJhbGciOiJSUzI1NiJ9.eyJqdGkiOiIwNWFjOGViMy1jMzM2LTRiODQtYTdjYy0yOWY4ZjVlOTlkNDciLCJzdWIiOiI2MjQxYTkwOTY5OTY0OTAwNmFlNGY3YmEiLCJuYmYiOjE2OTgzMTYyMjMsImlzcyI6Imh0dHBzOi8vYXV0aC5hdGxhc3NpYW4uY29tIiwiaWF0IjoxNjk4MzE2MjIzLCJleHAiOjE2OTgzMTk4MjMsImF1ZCI6IkZEczJYV3d4cW0zTlVDMUhDaUp0bnU3OGlRdDRiQXBaIiwiaHR0cHM6Ly9pZC5hdGxhc3NpYW4uY29tL3Nlc3Npb25faWQiOiJkZDM5Nzg2OS01MTAxLTQ2OGYtOGIyMi0zNzg2ODAyN2JmNjgiLCJjbGllbnRfaWQiOiJGRHMyWFd3eHFtM05VQzFIQ2lKdG51NzhpUXQ0YkFwWiIsImh0dHBzOi8vaWQuYXRsYXNzaWFuLmNvbS91anQiOiIwNTA4OThiMi1iOWJiLTQ2MDAtYTBlZi0zM2U1NGY2YWJjZWMiLCJodHRwczovL2lkLmF0bGFzc2lhbi5jb20vYXRsX3Rva2VuX3R5cGUiOiJBQ0NFU1MiLCJodHRwczovL2F0bGFzc2lhbi5jb20vZmlyc3RQYXJ0eSI6ZmFsc2UsInNjb3BlIjoicmVhZDphcHBsaWNhdGlvbi1yb2xlOmppcmEgd3JpdGU6YXR0YWNobWVudDpqaXJhIHdyaXRlOmNvbW1lbnQ6amlyYSByZWFkOnByb2plY3Q6amlyYSByZWFkOnByb2plY3QtY2F0ZWdvcnk6amlyYSB3cml0ZTpwcm9qZWN0LWNhdGVnb3J5OmppcmEgd3JpdGU6aXNzdWUtdHlwZTpqaXJhIHdyaXRlOmlzc3VlOmppcmEgcmVhZDpwcm9qZWN0LXR5cGU6amlyYSB3cml0ZTpjb21tZW50LnByb3BlcnR5OmppcmEgcmVhZDppc3N1ZS10eXBlLWhpZXJhcmNoeTpqaXJhIHJlYWQ6dXNlcjpqaXJhIHJlYWQ6YXZhdGFyOmppcmEgcmVhZDppc3N1ZS10eXBlOmppcmEgcmVhZDppc3N1ZTpqaXJhIHJlYWQ6cHJvamVjdC5lbWFpbDpqaXJhIHJlYWQ6cHJvamVjdC5wcm9wZXJ0eTpqaXJhIHJlYWQ6cHJvamVjdC5jb21wb25lbnQ6amlyYSB3cml0ZTpwcm9qZWN0OmppcmEgcmVhZDpncm91cDpqaXJhIGRlbGV0ZTpwcm9qZWN0OmppcmEgcmVhZDpwcm9qZWN0LXZlcnNpb246amlyYSIsImh0dHBzOi8vYXRsYXNzaWFuLmNvbS92ZXJpZmllZCI6dHJ1ZSwiaHR0cHM6Ly9hdGxhc3NpYW4uY29tL2VtYWlsRG9tYWluIjoib29kbGVzLmlvIiwiaHR0cHM6Ly9pZC5hdGxhc3NpYW4uY29tL3Byb2Nlc3NSZWdpb24iOiJ1cy13ZXN0LTIiLCJodHRwczovL2F0bGFzc2lhbi5jb20vb2F1dGhDbGllbnRJZCI6IkZEczJYV3d4cW0zTlVDMUhDaUp0bnU3OGlRdDRiQXBaIiwiaHR0cHM6Ly9hdGxhc3NpYW4uY29tL3N5c3RlbUFjY291bnRFbWFpbCI6ImNmMTdjNTQ2LWEyM2QtNDQ0Yi05YTllLThlMWY4Mjg2NmI3YUBjb25uZWN0LmF0bGFzc2lhbi5jb20iLCJodHRwczovL2F0bGFzc2lhbi5jb20vM2xvIjp0cnVlLCJodHRwczovL2F0bGFzc2lhbi5jb20vc3lzdGVtQWNjb3VudElkIjoiNzEyMDIwOmIxN2M2Y2UyLTdhNDItNGQwNi1hZWI2LWQ2MmY1OThmMzc3ZSIsImh0dHBzOi8vaWQuYXRsYXNzaWFuLmNvbS92ZXJpZmllZCI6dHJ1ZSwiaHR0cHM6Ly9hdGxhc3NpYW4uY29tL3N5c3RlbUFjY291bnRFbWFpbERvbWFpbiI6ImNvbm5lY3QuYXRsYXNzaWFuLmNvbSJ9.HrAZPzS6ZJ8hvE9IlNWQBkpcSXYPKE_adc_KhGxhAj-J-yXz9G1AhxQN2-RqFDaRYYm1lcgzPhxLiQ5k-wtMtticvYqV9Zvlydn99TK8F4Nr40C5RYgjB3h2kdhJnAmKw6Ik4jTa6gS-mVFIOeH7lk8FNVYT8NSQgWdsfIlnQPAdfR0VBr2i33GkN7wIGsDkGIpaBES8LsKpfwHJ-b4OKZ6WbMPSz3H9KG9YEYavd5eeVydSDdDBru2_e6Sw08DReIFquuIqxtUyI9e7XRvbL2-hydtmLMF83N4rijgYEGcPlvuOvBcN1iigfxSDqUOtJYzu7xGEY4CBsuhP7Kl6Jg"
cloud_id = "0c546bda-855f-44d6-ba91-271f0a0b1a17"
Authorization="ATATT3xFfGF0P1owAyQgoJnAmHM7h5hiV28hO2WXKdTsJ989ZflUxUSK-88U9GtObEq5WuhokAYiPXd3WS8RJvZV8500gJlt27YDPOP8QuBzvg5LQgVtrq6HFQgTRm4drLHoZWv7XMfG92sCp4vUjOR4Yevr3F0sZEuVTwpkCkMl5nrHK-VHLto=396B0E47"
# Get Project Type
# def get_project_type():
#     url = "http://127.0.0.1:8000/jira-management/jira-project-type/"
#     query = {
#              "cloud_id":cloud_id,
#              }
    
#     headers = {"Accept": "application/json",'Authorization': Authorization}
#     response = requests.request("GET",url,headers=headers,data=query)
#     response_data = response.json()
#     return response_data
    
# class GetProjectTypeTool(BaseTool):
#     name = "get_project_type"
#     description = "This tool provides the project type for jira project.Also verify the provided project type is valid or not by checking in output json"
    
#     def _run(self,query:str)->json:
#         response = get_project_type()
#         return response
    
#     async def _arun(self,query:str)->str:
#         raise NotImplementedError("not supported")  

# # Get Project Lead
# def get_project_lead():
#     url = "http://127.0.0.1:8000/jira-management/jira-project-lead/"
#     query = {
#              "cloud_id":cloud_id,
#              }
#     headers = {"Accept": "application/json",'Authorization': Authorization}
#     response = requests.request("GET",url,headers=headers,data=query)
#     response_data = response.json()
#     return response_data

# class GetProjectLeadTool(BaseTool):
#     name = "get_project_lead"
#     description = "This tool provides the user account id by email address for jira project."
    
#     def _run(self,query:str)->json:
#         response = get_project_lead()
#         return response
    
#     async def _arun(self,query:str)->str:
#         raise NotImplementedError("not supported")  
    
# User Authentication
import webbrowser
def user_authentication():
    url1 = """https://auth.atlassian.com/authorize?audience=api.atlassian.com&
    client_id=FDs2XWwxqm3NUC1HCiJtnu78iQt4bApZ&
    scope=offline_access write:project:jira delete:project:jira read:project-category:jira write:project-category:jira read:project.email:jira read:project:jira read:issue-type:jira read:project.property:jira read:user:jira read:application-role:jira read:avatar:jira read:group:jira read:issue-type-hierarchy:jira read:project-version:jira read:project.component:jira write:issue:jira write:comment:jira write:comment.property:jira write:attachment:jira read:issue:jira write:issue-type:jira read:project-type:jira&
    redirect_uri=http://127.0.0.1:8000/jira-management/jira-auth/&
    state=${"""
    url2 = """YOUR_USER_BOUND_VALUE}&response_type=code&prompt=consent"""
    url = "".join([url1,url2])
    # Open the default browser in a new tab.
    webbrowser.open(url)

    # Wait for the user to be redirected back to the application.
    # The `JiraAuthView` class will automatically exchange the response code for an access token and save it to the `access_token.txt` file.

    # Once the user has been redirected back to the application, you can use the following code to get the access token from the `access_token.txt` file:

    with open("langchain/JiraTools/access_token.json", "r") as f:
        data = f.read()
    return data

# class AuthenticationInputSchema(BaseModel):
#     email: str = Field(description="It should be an user email")
#     password: str = Field(description="It should be an user password")

 
class AuthenticateUserTool(BaseTool):
    name = "user_authentication_tool"
    description = """The Jira user authentication tool is a custom tool that authenticates 
    the user and gets the authentication token."""
    # description = """The Jira user authentication tool is a custom tool that authenticates 
    # the user and generates an authentication bearer token with an access data dictionary.
    # The access data dictionary contains the cloud ID and API base domain of the Jira instance."""
    # args_schema: Type[AuthenticationInputSchema] = AuthenticationInputSchema
    
    def _run(self,query:str)->json:
        data = user_authentication()
        return data
    async def _arun(self,query:str)->str:
        raise NotImplementedError("not supported")  
    
    
# Create Project
def create_project(key,name,project_type,username):
    url = "http://127.0.0.1:8000/jira-management/jira-project/"
    query = {
            "cloud_id":cloud_id,
            "key": key,
            "name": name,
            "project_type":project_type,
            "username": username
             }
    headers = {"Accept": "application/json",'Authorization': Authorization}
    response = requests.request("POST",url,headers=headers,data=query)
    response_data = response.json()
    return response_data

class ProjectCreationInputSchema(BaseModel):
    key: str = Field(description="It is represent as a key for the project and should be in uppercase .")
    name: str = Field(description="It should be name of the project")
    project_type: str = Field(description="It should be the type of project.")
    username : str = Field(description="It should be the project lead email or fullname ")
    
class CreateProjectTool(BaseTool):
    name = "create_project"
    description = """The Jira project creation tool is a custom tool that 
    creates a project in Jira with the required project type, project lead, 
    and project name. If the project key is not provided by the user,
    the tool will generate a key from the project name in uppercase.
     If the project name is not provided by the user,ask the user to provide the project name.
    If the project_type is not provided by the user,ask the user to provide the project type.
    If the project_lead is not provided by the user,ask the user to provide the email or full name for project lead."""
    args_schema: Type[ProjectCreationInputSchema] = ProjectCreationInputSchema
    def _run(self,key:str,name:str,project_type:str,username:str)->json:
        response = create_project(key,name,project_type,username)
        return response
    
    async def _arun(self,key:str,name:str,project_type:str,username:str)->str:
        raise NotImplementedError("not supported")  
   

# Create Issue
def create_issue(key,duedate,description,summary,issuetype):
    url = "http://127.0.0.1:8000/jira-management/jira-issue/"
    query = {
            "cloud_id":cloud_id,
            "key": key,
            "duedate": duedate,
            "description":description,
            "summary":summary,
            "issuetype": issuetype
            }
    headers = {"Accept": "application/json",'Authorization': Authorization}
    response = requests.request("POST",url,headers=headers,data=query)
    response_data = response.json()
    return response_data

class IssueCreationInputSchema(BaseModel):
    key: str = Field(description="It is represent as a key for the project and should be in uppercase .")
    duedate: str = Field(description="It should be the due date for this issue")
    description: str = Field(description="It should be the description of the issue.")
    summary : str = Field(description="It should be the short summary of the issue.which is created using description")
    issuetype : str = Field(description="""It should be the type of the issue.Issuetype chooses are: Sub-task, Email request,
                                Submit a request or incident, Ask a question, New Issue Type, IT Help,
                                Service Request, Service Request with Approvals, Task, Improvement, New Feature, Bug, Story, name, Epic.""")
    
class CreateIssueTool(BaseTool):
    name = "create_issue"
    description = """A custom tool that creates issues in Jira with the required details, 
    including project key, due date, description. 
    If project key, duedate and description are not provided by the user,Ask them about the details and 
    select an issuetype based on the description. Issuetype chooses are: Sub-task, Email request,
    Submit a request or incident, Ask a question, New Issue Type, IT Help, Service Request, Service Request with Approvals, Task, Improvement, New Feature, Bug, Story, name, Epic.
    Create a short summary text using description.
    """
    args_schema: Type[IssueCreationInputSchema] = IssueCreationInputSchema
    def _run(self,key:str,duedate:str,description:str,summary:str,issuetype:str)->json:
        response = create_issue(key,duedate,description,summary,issuetype)
        return response
    
    async def _arun(self,key:str,duedate:str,description:str,summary:str,issuetype:str)->str:
        raise NotImplementedError("not supported") 
    
# Create Issue
def assign_issue(key,username):
    url = "http://127.0.0.1:8000/jira-management/jira-issue-assign/"
    query = {
            "cloud_id":cloud_id,
            "key": key,
            "username": username,
            }
    headers = {"Accept": "application/json",'Authorization': Authorization}
    response = requests.request("POST",url,headers=headers,data=query)
    response_data = response.json()
    return response_data

class AssignIssueInputSchema(BaseModel):
    key: str = Field(description="It should be a issue key with uppercase")
    username : str = Field(description="It should be the assignee email or fullname ")
    
class AssignIssueTool(BaseTool):
    name = "assign_issue"
    description = """This is a custom tool that 
    assign a issue to mentioned assignee with the required issue key and email. 
    If these issue key and email are not provided by the user, ask them to insert these details.
    """
    args_schema: Type[AssignIssueInputSchema] = AssignIssueInputSchema
    def _run(self,key:str,username:str)->json:
        response = assign_issue(key,username)
        return response
    
    async def _arun(self,key:str,username:str)->str:
        raise NotImplementedError("not supported") 