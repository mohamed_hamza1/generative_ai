import re
from Packages.Data_Scrapper import Scrapper_Agent

from Packages.Document.Unstructured.Excel import excel_with_agent
from Packages.Document.Unstructured.PDF import pdf_with_agent
from Packages.Document.Unstructured.Word import word_with_agent 
from Packages.Document.Unstructured.Powerpoint import powerpoint_with_agent
from Packages.Document.Structured import structured_documents_agent

# Regular expression pattern for a URL
url_pattern = re.compile(
    r'https?://'  # Start with http:// or https://
    r'(?:[a-zA-Z0-9-]+\.)*'  # Domain name prefix
    r'(?:[a-zA-Z0-9-]+\.[a-zA-Z]{2,})'  # Domain name
    r'(?:/[^\s"<>#]*)?'  # Path
    r'(?:\?[^\s"<>#]*)?'  # Query string
    r'(?:#[^\s"<>]*)?',  # Fragment identifier
    re.IGNORECASE)
# Regular expression pattern for a PDF URL
pdf_pattern = re.compile(
    r'^.*\.pdf$', re.IGNORECASE)
# Regular expression pattern for a PDF URL
word_pattern = re.compile(
    r'^.*\.docx$', re.IGNORECASE)
# Regular expression pattern for a PDF URL
excel_pattern = re.compile(
    r'^.*\.xlsx$', re.IGNORECASE)
# Regular expression pattern for a PDF URL
power_point_pattern = re.compile(
    r'^.*\.ppt$', re.IGNORECASE)

def find_url(text):
    # Find all matches in the text
    urls = re.findall(url_pattern, text)
    return urls[0] if urls else None

def is_pdf_url(url):
    return re.match(pdf_pattern, url) is not None
def is_word_url(url):
    return re.match(word_pattern, url) is not None
def is_excel_url(url):
    return re.match(excel_pattern, url) is not None
def is_powerpoint_url(url):
    return re.match(power_point_pattern, url) is not None
class ask_bot:
   def ask_user(user_input):
    try:
        while True:
            # user_input = input("Please enter your sentence with a URL: ")

            while True:
                user_input_url = ""
                url = find_url(user_input)
                if not url:
                    print("No valid URL found in the input. Please try again.")
                    user_input_url += input("Please enter valid a URL: ")
                    user_input += user_input_url 
                    continue
                
                choice = input("Do you want to extract data from a website or a document? (website/document): ").strip().lower()

                if choice == "website":
                    print("Scraping website...")
                    Scrapper_Agent.Scrapper_Agent(user_input)
                    return
                elif choice == "document":
                    doc_type = input("Is it a structured or unstructured document? (structured/unstructured): ").strip().lower()
                    if doc_type == "structured":
                        # if is_pdf_url(url):
                        #     print("Valid PDF URL provided.")
                            print(structured_documents_agent.structured_documents_agent_r(user_input))
                            return
                        # else:
                        #     print("This is not a valid PDF URL.")
                        #     continue
                    elif doc_type == "unstructured":
                        while True:
                            doc_format = input("Is the provided document an Excel, PDF, Word, or PowerPoint? (excel/pdf/word/powerpoint): ").strip().lower()
                            if doc_format in ["excel", "pdf", "word", "powerpoint"]:
                                print(f"Processing a {doc_format.capitalize()} document...")
                                # is_valid_doc = globals().get(f"is_{doc_format}_url")
                                # if is_valid_doc(url):
                                #     print(f"Valid PDF {doc_format} provided.")
                                    # Call the appropriate agent function based on the doc_format
                                    # For example, if the agent function is named after the format:
                                if(doc_format=="excel"):
                                    print(excel_with_agent.excel_agent_r(user_input))
                                elif(doc_format=="pdf"):
                                    print(pdf_with_agent.pdf_agent_r(user_input))
                                elif(doc_format=="word"):
                                    print(word_with_agent.word_agent_r(user_input))
                                elif(doc_format=="powerpoint"):
                                    print(powerpoint_with_agent.powerpoint_agent_r(user_input))
                                # agent_function = globals().get(f"{doc_format}_with_agent").get(f"{doc_format}_agent_r")
                                # print(agent_function(user_input))
                                return
                                # else:
                                #     print(f"This is not a valid {doc_format} URL.")
                                #     continue
                            else:
                                print("Invalid document format. Please try again.")
                    else:
                        print("Invalid choice for document type. Please try again.")
                        continue
                else:
                    print("Invalid choice for extraction type. Please try again.")
                    continue
    except KeyboardInterrupt:
        print("\nExiting the program.")
        
if __name__ == '__main__':
    # ask_bot.ask_user("hello")    
    print(find_url("Please enter valid a URL: https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf"))